/******/ (() => {
    // webpackBootstrap
    /******/ "use strict";
    var __webpack_exports__ = {};
    /*!****************************************************************!*\
  !*** ../demo1/src/js/pages/crud/ktdatatable/base/data-ajax.js ***!
  \****************************************************************/

    // Class definition

    var KTDatatableRemoteAjaxDemo = (function () {
        // Private functions

        // basic demo
        var demo = function () {
            var datatable = $("#kt_datatable").KTDatatable({
                // datasource definition
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: url_companyList,
                            method: "GET",
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== "undefined") {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    saveState: false,
                },

                // layout definition
                layout: {
                    scroll: false,
                    footer: false,
                },

                // column sorting
                sortable: true,

                pagination: true,

                search: {
                    input: $("#kt_datatable_search_query"),
                    key: "generalSearch",
                },

                // columns definition
                columns: [
                    {
                        field: "company",
                        title: "Company",
                        // sortable: "asc",
                        // width: 30,
                        // type: "number",
                        // selector: false,
                        // textAlign: "center",
                    },
                    {
                        field: "person_incharge",
                        title: "Person Incharge",
                    },
                    {
                        field: "email",
                        title: "Email",
                    },
                    {
                        field: "position",
                        title: "Position",
                    },
                    {
                        field: "contact_no",
                        title: "Contact No",
                    },
                    {
                        field: "address",
                        title: "Address",
                    },
                    {
                        field: "level",
                        title: "Level",
                    },
                    {
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        width: 125,
                        overflow: "visible",
                        textAlign: "left",
                        autoHide: false,
                        template: function (row) {
                            return (
                                '\
                                <button modal_row_id="' +
                                row.id +
                                '" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                                    <span class="svg-icon svg-icon-md" id="edit_details">\
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                                <rect x="0" y="0" width="24" height="24"/>\
                                                <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                                <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                            </g>\
                                        </svg>\
                                    </span>\
                                </button>\
                                <button delete_row_id="' +
                                row.id +
                                '" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                                    <span class="svg-icon svg-icon-md">\
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                                <rect x="0" y="0" width="24" height="24"/>\
                                                <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                                <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                            </g>\
                                        </svg>\
                                    </span>\
                                </button>\
                            '
                            );
                        },
                    },
                ],
            });

            $("#kt_datatable_search_level").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "level");
            });

            // click on edit show modal

            var row_id = "";
            var addNew = false;

            const form = document.getElementById("form-update");
            var validator = FormValidation.formValidation(form, {
                fields: {
                    company: {
                        validators: {
                            notEmpty: {
                                message: "Company Name Is Required",
                            },
                        },
                    },
                    person_incharge: {
                        validators: {
                            notEmpty: {
                                message: "Person In Charege Is Required",
                            },
                        },
                    },
                    contact_no: {
                        validators: {
                            notEmpty: {
                                message: "Contact No Is Required",
                            },
                        },
                    },
                    address: {
                        validators: {
                            notEmpty: {
                                message: "Address Is Required",
                            },
                        },
                    },
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        // rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: "",
                    }),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            });

            datatable.on("click", "[modal_row_id]", function () {
                // document.getElementById("form-update").reset();
                validator.resetForm(true);
                $("#modal_title").html("");
                $("#update_details_btn").html("Edit");
                $("#modal_title").append("Update Company Details");
                addNew = false;
                row_id = $(this).attr("modal_row_id");
                console.log(row_id);

                $.ajax({
                    type: "POST",
                    url: url_showDetails,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        row_id: row_id,
                    },
                    success: function (res) {
                        console.log(res);

                        $("#company-input").val(res.company);
                        $("#person-input").val(res.person_incharge);
                        $("#email-input").val(res.email);
                        $("#position-input").val(res.position);
                        $("#contact_no-input").val(res.contact_no);
                        $("#level-input").val(res.level);
                        $("#address-input").val(res.address);

                        $("#kt_datatable_modal").modal("show");
                    },
                });
            });

            //update or add new detail
            $("#update_details_btn").click(function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Success!",
                    text: "Company List Updated",
                    icon: "success",
                    showConfirmButton: false,
                    timer: 1000,
                });
                var company = $("#company-input").val();
                var person_incharge = $("#person-input").val();
                var email = $("#email-input").val();
                var position = $("#position-input").val();
                var contact_no = $("#contact_no-input").val();
                var level = $("#level-input").val();
                var address = $("#address-input").val();

                console.log(row_id);

                if (validator) {
                    validator.validate().then(function (status) {
                        if (status == "Valid") {
                            $.ajax({
                                type: "POST",
                                url: url_updateDetails,
                                cache: false,
                                dataType: "json",
                                data: {
                                    _token: $('meta[name="_token"]').attr(
                                        "content"
                                    ),
                                    row_id: row_id,
                                    addNew: addNew,
                                    company: company,
                                    person_incharge: person_incharge,
                                    email: email,
                                    position: position,
                                    contact_no: contact_no,
                                    level: level,
                                    address: address,
                                },
                            });
                            $("#kt_datatable_modal").modal("hide");
                            // this does not work for value="", need to set it to the null again
                            $("#form-update").trigger("reset");
                            datatable.reload();
                        } else {
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                },
                            });
                        }
                    });
                }
            });

            // click on delete button
            datatable.on("click", "[delete_row_id]", function () {
                var row_id = $(this).attr("delete_row_id");
                console.log(row_id);

                Swal.fire({
                    text: "Delete Confirmation",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Delete",
                    cancelButtonText: "Cancel",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light",
                    },
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: url_deleteDetails,
                            cache: false,
                            dataType: "json",
                            data: {
                                _token: $('meta[name="_token"]').attr(
                                    "content"
                                ),
                                row_id: row_id,
                            },
                        });
                        datatable.reload();
                    } else if (result.dismiss === "cancel") {
                        Swal.fire({
                            text: "Record Was Not Deleted!",
                            icon: "error",
                            buttonsStyling: false,
                            // confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            });

            $("#new_record").click(function (event) {
                event.preventDefault();
                $("#update_details_btn").html("Add");
                document.getElementById("form-update").reset();
                validator.resetForm(true);
                $("#modal_title").html("");
                $("#modal_title").append("New Company Details");
                addNew = true;

                $("#kt_datatable_modal").modal("show");
            });

            $("#kt_datatable_search_level").selectpicker();
        };

        return {
            // public functions
            init: function () {
                demo();
            },
        };
    })();

    jQuery(document).ready(function () {
        KTDatatableRemoteAjaxDemo.init();
    });

    /******/
})();
//# sourceMappingURL=data-ajax.js.map
