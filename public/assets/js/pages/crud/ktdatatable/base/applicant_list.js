/******/ (() => {
    // webpackBootstrap
    /******/ "use strict";
    var __webpack_exports__ = {};
    /*!****************************************************************!*\
  !*** ../demo1/src/js/pages/crud/ktdatatable/base/data-ajax.js ***!
  \****************************************************************/

    // Class definition

    var KTDatatableRemoteAjaxDemo = (function () {
        // Private functions

        // basic demo
        var demo = function () {
            var datatable = $("#kt_datatable").KTDatatable({
                // datasource definition
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: url_applicantList,
                            method: "GET",
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== "undefined") {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    saveState: false,
                },

                // layout definition
                layout: {
                    scroll: false,
                    footer: false,
                },

                // column sorting
                sortable: true,

                pagination: true,

                search: {
                    input: $("#kt_datatable_search_query"),
                    key: "generalSearch",
                },

                // columns definition
                columns: [
                    {
                        field: "studentID",
                        title: "Student_ID",
                        // sortable: "asc",
                        // width: 30,
                        // type: "number",
                        // selector: false,
                        // textAlign: "center",
                    },
                    {
                        field: "name",
                        title: "Name",
                    },
                    {
                        field: "programme",
                        title: "Programme",
                    },
                    {
                        field: "app_status",
                        title: "Application Status",
                        template: function (row) {
                            // console.log(row.reg_status);
                            var status = {
                                submitted: {
                                    title: "Submitted",
                                    class: " label-light-success",
                                },
                                pending: {
                                    title: "Pending",
                                    class: " label-light-danger",
                                },
                            };
                            return (
                                '<span class="label font-weight-bold label-lg ' +
                                status[row.app_status].class +
                                ' label-inline">' +
                                status[row.app_status].title +
                                "</span>"
                            );
                        },
                    },
                    {
                        field: "review_status",
                        title: "Review Status",
                        template: function (row) {
                            // console.log(row.reg_status);
                            var status = {
                                reviewed: {
                                    title: "Reviewed",
                                    class: " label-light-success",
                                },
                                pending: {
                                    title: "Pending",
                                    class: " label-light-danger",
                                },
                            };
                            return (
                                '<span class="label font-weight-bold label-lg ' +
                                status[row.review_status].class +
                                ' label-inline">' +
                                status[row.review_status].title +
                                "</span>"
                            );
                        },
                    },
                    {
                        field: "Actions",
                        title: "Actions",
                        sortable: false,
                        width: 125,
                        overflow: "visible",
                        textAlign: "left",
                        autoHide: false,
                        template: function (row) {
                            return (
                                '\
                                <button modal_row_id="' +
                                row.id +
                                '" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                                    <span class="svg-icon svg-icon-md">\
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                                <polygon points="0 0 24 0 24 24 0 24" />\
                                                <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />\
                                                <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />\
                                            </g>\
                                        </svg>\
                                      </span>\
                                </button>\
                                <button map_row_id="' +
                                row.id +
                                '" class="btn btn-sm btn-clean btn-icon" title="Map">\
                                    <span class="svg-icon svg-icon-md">\
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                                <rect x="0" y="0" width="24" height="24" />\
                                                <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero" />\
                                            </g>\
                                        </svg>\
                                    </span>\
                                </button>\
                                <button logbook_row_id="' +
                                row.id +
                                '" class="btn btn-sm btn-clean btn-icon" title="Logbook">\
                                    <span class="svg-icon svg-icon-md">\
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                                <rect x="0" y="0" width="24" height="24" />\
                                                <path d="M13.6855025,18.7082217 C15.9113859,17.8189707 18.682885,17.2495635 22,17 C22,16.9325178 22,13.1012863 22,5.50630526 L21.9999762,5.50630526 C21.9999762,5.23017604 21.7761292,5.00632908 21.5,5.00632908 C21.4957817,5.00632908 21.4915635,5.00638247 21.4873465,5.00648922 C18.658231,5.07811173 15.8291155,5.74261533 13,7 C13,7.04449645 13,10.79246 13,18.2438906 L12.9999854,18.2438906 C12.9999854,18.520041 13.2238496,18.7439052 13.5,18.7439052 C13.5635398,18.7439052 13.6264972,18.7317946 13.6855025,18.7082217 Z" fill="#000000" />\
                                                <path d="M10.3144829,18.7082217 C8.08859955,17.8189707 5.31710038,17.2495635 1.99998542,17 C1.99998542,16.9325178 1.99998542,13.1012863 1.99998542,5.50630526 L2.00000925,5.50630526 C2.00000925,5.23017604 2.22385621,5.00632908 2.49998542,5.00632908 C2.50420375,5.00632908 2.5084219,5.00638247 2.51263888,5.00648922 C5.34175439,5.07811173 8.17086991,5.74261533 10.9999854,7 C10.9999854,7.04449645 10.9999854,10.79246 10.9999854,18.2438906 L11,18.2438906 C11,18.520041 10.7761358,18.7439052 10.4999854,18.7439052 C10.4364457,18.7439052 10.3734882,18.7317946 10.3144829,18.7082217 Z" fill="#000000" opacity="0.3" />\
                                            </g>\
                                        </svg>\
                                    </span>\
                                </button>\
                            '
                            );
                        },
                    },
                ],
            });

            $("#kt_datatable_search_programme").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "programme");
            });

            $("#kt_datatable_supervisor_toggle").on("change", function () {
                if ($("#kt_datatable_supervisor_toggle").is(":checked")) {
                    console.log($(this).val().toLowerCase());
                    datatable.search(
                        $(this).val().toLowerCase(),
                        "supervisor_toggle"
                    );
                } else {
                    datatable.search("off", "supervisor_toggle");
                }
            });

            // click on edit show modal
            var row_id = "";
            var isNew = "";

            datatable.on("click", "[modal_row_id]", function () {
                row_id = $(this).attr("modal_row_id");
                console.log(row_id);
                $("#row_jobDesc").hide();

                $.ajax({
                    type: "POST",
                    url: url_ApplicantData,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        row_id: row_id,
                    },
                    success: function (res) {
                        isNew = res.isNew;

                        if (res.company.pdf_path) {
                            $("#kt_apps_contacts_view_tab_3").html("");
                            var url_offer =
                                offer_asset + "/" + res.company.pdf_path;

                            var offerLetter =
                                '<iframe src="' +
                                url_offer +
                                ' " frameborder="0" width="1100" height="600"></iframe>';
                            $("#kt_apps_contacts_view_tab_3").append(
                                offerLetter
                            );
                        } else {
                            $("#kt_apps_contacts_view_tab_3").html("");
                            var no_offer_letter =
                                '<div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12"><h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!</h1><p class="font-weight-boldest display-4">No Offer Letter Uploaded.</p></div>';
                            $("#kt_apps_contacts_view_tab_3").append(
                                no_offer_letter
                            );
                        }

                        fillCompanyData(res);
                        fillStudentData(res);

                        $("#kt_datatable_review_modal").modal("show");
                    },
                    error: function (res) {
                        console.log("sdfdfds");
                        Swal.fire({
                            text: "No Application Submitted",
                            icon: "error",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    },
                });
            });

            datatable.on("click", "[map_row_id]", function () {
                row_id = $(this).attr("map_row_id");
                var type = "map";
                console.log(row_id);

                $.ajax({
                    type: "POST",
                    url: url_ApplicantData,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        row_id: row_id,
                        type: type,
                    },
                    success: function (res) {
                        // console.log(res.lat);

                        $("#kt_map_card").html("");

                        var map =
                            '<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDXf5G5L1zomTZXwpt_kogrSUl93xCkozo&q=' +
                            res.lat +
                            "," +
                            res.lng +
                            '" style="border:0" loading="lazy" width="1100" height="678"></iframe>';
                        $("#kt_map_card").append(map);

                        $("#kt_datatable_map_modal").modal("show");
                    },
                    error: function (res) {
                        Swal.fire({
                            text: "Application Not Submitted",
                            icon: "error",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    },
                });
            });

            datatable.on("click", "[logbook_row_id]", function () {
                row_id = $(this).attr("logbook_row_id");
                var type = "logbook";
                console.log(row_id);

                $.ajax({
                    type: "POST",
                    url: url_ApplicantData,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        row_id: row_id,
                        type: type,
                    },
                    success: function (res) {
                        console.log("here");
                        console.log(res);
                        if (res == "empty") {
                            Swal.fire({
                                text: "No Logbook Submitted",
                                icon: "error",
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        } else {
                            window.location.href = url_logbook + "/" + row_id;
                        }
                    },
                });
            });

            function fillCompanyData(res) {
                $("#company_org").html("");
                $("#company_email").html("");
                $("#company_contact").html("");
                $("#company_address").html("");
                $("#company_supervisor").html("");
                $("#company_designation").html("");
                $("#company_internDesignation").html("");
                $("#company_start").html("");
                $("#company_end").html("");
                $("#company_allowance").html("");
                $("#company_jobDesc").html("");

                $("#company_org").append(res.company.organisation_name);

                $("#company_email").append(
                    '<a href="mailto:' +
                        res.company.email +
                        '">' +
                        res.company.email +
                        "</a>"
                );

                $("#company_contact").append(res.company.contact_no);
                $("#company_address").append(res.company.address);
                $("#company_supervisor").append(res.company.site_supervisor);
                $("#company_designation").append(res.company.designation);
                $("#company_internDesignation").append(
                    res.company.intern_designation
                );
                $("#company_start").append(res.company.start_internship);
                $("#company_end").append(res.company.end_internship);
                $("#company_allowance").append(res.company.allowance);

                if (res.company.job_desc) {
                    $("#company_jobDesc").append(res.company.job_desc);
                    $("#row_jobDesc").show();
                }

                if (res.isNew == true) {
                    $("#org_title").html("");

                    var html =
                        "<span class='label label-inline font-weight-bold label-light-success'>New</span>&nbsp Organisation Name:";
                    $("#org_title").append(html);
                } else {
                    $("#org_title").html("");

                    $("#org_title").append("Organisation Name:");
                }
            }

            function fillStudentData(res) {
                $("#student_name").html("");
                $("#student_ID").html("");
                $("#student_programme").html("");
                $("#student_IC").html("");
                $("#student_email").html("");
                $("#student_phoneNum").html("");
                $("#student_address").html("");

                $("#student_name").append(res.student_name);
                $("#student_ID").append(res.student_details.studentID);
                $("#student_programme").append(res.student_details.programme);
                $("#student_IC").append(res.student_details.IC);
                $("#student_email").append(res.student_email);
                $("#student_phoneNum").append(res.student_details.phoneNum);
                $("#student_address").append(res.student_details.address);

                $("#kt_student_avatar").html("");

                var profileAvatar =
                    '<div class="image-input-wrapper" style="background-image: url(' +
                    path_avatar +
                    "/" +
                    res.student_details.profile_path +
                    ')"></div>';

                $("#kt_student_avatar").append(profileAvatar);
            }

            $("#btn_review").click(function (event) {
                $("#kt_datatable_review_modal").modal("hide");
                var type = "review";
                console.log("hahah");

                $.ajax({
                    type: "POST",
                    url: url_actionApplicant,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        row_id: row_id,
                        type: type,
                    },
                    success: function (res) {
                        datatable.reload();
                        Swal.fire({
                            text: "Application Reviewed",
                            icon: "success",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    },
                });
            });

            $("#btn_review_save").click(function (event) {
                var type = "review save";

                if (isNew == true) {
                    $("#kt_datatable_review_modal").modal("hide");

                    $.ajax({
                        type: "POST",
                        url: url_actionApplicant,
                        cache: false,
                        dataType: "json",
                        data: {
                            _token: $('meta[name="_token"]').attr("content"),
                            row_id: row_id,
                            type: type,
                        },
                        success: function (res) {
                            datatable.reload();

                            Swal.fire({
                                text: "Application Reviewed & Saved ",
                                icon: "success",
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        },
                    });
                } else {
                    Swal.fire({
                        text: "Company Already Exists",
                        icon: "warning",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                }
            });

            $("#kt_datatable_search_level").selectpicker();
        };

        return {
            // public functions
            init: function () {
                demo();
            },
        };
    })();

    jQuery(document).ready(function () {
        KTDatatableRemoteAjaxDemo.init();
    });

    /******/
})();
//# sourceMappingURL=data-ajax.js.map
