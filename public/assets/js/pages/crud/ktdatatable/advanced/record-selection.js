/******/
(() => {
    // webpackBootstrap
    /******/
    "use strict";
    var __webpack_exports__ = {};
    /*!***************************************************************************!*\
      !*** ../demo1/src/js/pages/crud/ktdatatable/advanced/record-selection.js ***!
      \***************************************************************************/

    // Class definition

    var KTDatatableRecordSelectionDemo = (function () {
        // Private functions

        var options = {
            // datasource definition
            data: {
                type: "remote",
                source: {
                    read: {
                        url: url_studentList,
                        method: "GET",
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== "undefined") {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                // pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: false,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            // columns definition
            columns: [
                {
                    field: "id",
                    title: "#",
                    sortable: false,
                    width: 20,
                    selector: true,
                    textAlign: "center",
                },
                {
                    field: "studentID",
                    title: "Student_ID",
                },
                {
                    field: "name",
                    title: "Name",
                },
                {
                    field: "programme",
                    title: "programme",
                },
                {
                    field: "reg_status",
                    title: "Registration Status",
                    autoHide: false,
                    // callback function support for column rendering
                    template: function (row) {
                        // console.log(row.reg_status);
                        var status = {
                            registered: {
                                title: "Registered",
                                class: " label-light-success",
                            },
                            pending: {
                                title: "Pending",
                                class: " label-light-danger",
                            },
                        };
                        return (
                            '<span class="label font-weight-bold label-lg ' +
                            status[row.reg_status].class +
                            ' label-inline">' +
                            status[row.reg_status].title +
                            "</span>"
                        );
                    },
                },
                {
                    field: "supervisor",
                    title: "Supervisor",
                    autoHide: false,
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.supervisor == "unassigned") {
                            var status = {
                                unassigned: {
                                    title: "Unassigned",
                                    state: "danger",
                                },
                            };
                            return (
                                '<span class="label label-' +
                                status[row.supervisor].state +
                                ' label-dot mr-2"></span><span class="font-weight-bold text-' +
                                status[row.supervisor].state +
                                '">' +
                                status[row.supervisor].title +
                                "</span>"
                            );
                        }
                        if (row.supervisor != "unassigned") {
                            var status = {
                                title: row.supervisor,
                                state: "success",
                            };
                            return (
                                '<span class="label label-' +
                                status.state +
                                ' label-dot mr-2"></span><span class="font-weight-bold text-' +
                                status.state +
                                '">' +
                                status.title +
                                "</span>"
                            );
                        }
                    },
                },
            ],
        };

        // basic demo
        var localSelectorDemo = function () {
            // enable extension
            options.extensions = {
                // boolean or object (extension options)
                checkbox: true,
            };

            options.search = {
                input: $("#kt_datatable_search_query"),
                key: "generalSearch",
            };

            var datatable = $("#kt_datatable").KTDatatable(options);

            $("#kt_datatable_search_programme").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "programme");
            });

            $("#kt_datatable_search_supervisor").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "supervisor");
            });

            $(".nav-item a").click(function () {
                var ids = datatable
                    .rows(".datatable-row-active")
                    .nodes()
                    .find('.checkbox > [type="checkbox"]')
                    .map(function (i, chk) {
                        return $(chk).val();
                    });
                var arr_selected = [];

                // append selected ids to array
                for (var i = 0; i < ids.length; i++) {
                    arr_selected.push(ids[i]);
                }

                var selected_supervisor = $(this).attr("selected_supervisor");
                // console.log(arr_selected);
                // console.log(selected_supervisor);

                $.ajax({
                    type: "POST",
                    url: url_assign_supervisor,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        arr_selected: arr_selected,
                        selected_supervisor: selected_supervisor,
                    },
                });
                Swal.fire({
                    title: "Success!",
                    text: "Student Details Edited",
                    icon: "success",
                    showConfirmButton: false,
                    timer: 1500,
                });

                datatable.reload();
                $("#kt_datatable_group_action_form").collapse("hide");
            });

            $("#kt_datatable_delete_all").click(function () {
                var ids = datatable
                    .rows(".datatable-row-active")
                    .nodes()
                    .find('.checkbox > [type="checkbox"]')
                    .map(function (i, chk) {
                        return $(chk).val();
                    });
                var arr_selected = [];
                for (var i = 0; i < ids.length; i++) {
                    arr_selected.push(ids[i]);
                }

                console.log(arr_selected);

                Swal.fire({
                    text: "Delete Confirmation",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Delete",
                    cancelButtonText: "Cancel",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light",
                    },
                }).then(function (result) {
                    if (result.value) {
                        Swal.fire({
                            title: "Success!",
                            text: "Student Details Edited",
                            icon: "success",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        $.ajax({
                            type: "POST",
                            url: url_delete_student,
                            dataType: "json",
                            data: {
                                _token: $('meta[name="_token"]').attr(
                                    "content"
                                ),
                                arr_selected: arr_selected,
                            },
                            // success: function (res) {
                            //     console.log(res);
                            // },
                        });

                        datatable.reload();
                        $("#kt_datatable_group_action_form").collapse("hide");
                    } else if (result.dismiss === "cancel") {
                        Swal.fire({
                            text: "Records Were Not Deleted!",
                            icon: "error",
                            buttonsStyling: false,
                            // confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            });

            $(
                "#kt_datatable_search_programme, #kt_datatable_search_supervisor"
            ).selectpicker();

            // show modal if record is seleceted
            datatable.on(
                "datatable-on-check datatable-on-uncheck",
                function (e) {
                    var checkedNodes = datatable
                        .rows(".datatable-row-active")
                        .nodes();
                    var count = checkedNodes.length;
                    $("#kt_datatable_selected_records").html(count);
                    if (count > 0) {
                        $("#kt_datatable_group_action_form").collapse("show");
                    } else {
                        $("#kt_datatable_group_action_form").collapse("hide");
                    }
                }
            );

            $("#kt_datatable_fetch_modal")
                .on("show.bs.modal", function (e) {
                    var ids = datatable
                        .rows(".datatable-row-active")
                        .nodes()
                        .find('.checkbox > [type="checkbox"]')
                        .map(function (i, chk) {
                            return $(chk).val();
                        });
                    console.log(ids);
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < ids.length; i++) {
                        var li = document.createElement("li");
                        li.setAttribute("data-id", ids[i]);
                        li.innerHTML = "Selected record ID: " + ids[i];
                        c.appendChild(li);
                    }
                    $("#kt_datatable_fetch_display").append(c);
                })
                .on("hide.bs.modal", function (e) {
                    $("#kt_datatable_fetch_display").empty();
                });
        };

        var serverSelectorDemo = function () {
            // enable extension
            options.extensions = {
                // boolean or object (extension options)
                checkbox: true,
            };
            options.search = {
                input: $("#kt_datatable_search_query_2"),
                key: "generalSearch",
            };

            var datatable = $("#kt_datatable_2").KTDatatable(options);

            $("#kt_datatable_search_status_2").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "programme");
            });

            $("#kt_datatable_search_type_2").on("change", function () {
                datatable.search($(this).val().toLowerCase(), "Type");
            });

            $(
                "#kt_datatable_search_status_2, #kt_datatable_search_type_2"
            ).selectpicker();

            datatable.on("datatable-on-click-checkbox", function (e) {
                // datatable.checkbox() access to extension methods
                var ids = datatable.checkbox().getSelectedId();
                var count = ids.length;

                $("#kt_datatable_selected_records_2").html(count);

                if (count > 0) {
                    $("#kt_datatable_group_action_form_2").collapse("show");
                } else {
                    $("#kt_datatable_group_action_form_2").collapse("hide");
                }
            });

            $("#kt_datatable_fetch_modal_2")
                .on("show.bs.modal", function (e) {
                    var ids = datatable.checkbox().getSelectedId();
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < ids.length; i++) {
                        var li = document.createElement("li");
                        li.setAttribute("data-id", ids[i]);
                        li.innerHTML = "Selected record ID: " + ids[i];
                        c.appendChild(li);
                    }
                    $("#kt_datatable_fetch_display_2").append(c);
                })
                .on("hide.bs.modal", function (e) {
                    $("#kt_datatable_fetch_display_2").empty();
                });
        };

        return {
            // public functions
            init: function () {
                localSelectorDemo();
                serverSelectorDemo();
            },
        };
    })();

    jQuery(document).ready(function () {
        KTDatatableRecordSelectionDemo.init();
    });

    /******/
})();
//# sourceMappingURL=record-selection.js.map
