"use strict";

var KTCalendarListView = (function () {
    return {
        //main function to initiate the module
        init: function () {
            var todayDate = moment().startOf("day");
            var YM = todayDate.format("YYYY-MM");
            var YESTERDAY = todayDate
                .clone()
                .subtract(1, "day")
                .format("YYYY-MM-DD");
            var TODAY = todayDate.format("YYYY-MM-DD");
            var TOMORROW = todayDate.clone().add(1, "day").format("YYYY-MM-DD");

            // *********************************************************************
            var id = "";
            var startDate = "";
            var endDate = "";
            var type = "";
            var className = "";

            // *********************************************************************

            var calendarEl = document.getElementById("kt_calendar");
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ["interaction", "dayGrid", "timeGrid", "list"],

                isRTL: KTUtil.isRTL(),
                header: {
                    left: "prev,next today",
                    center: "title",
                    right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
                },

                height: 800,
                contentHeight: 750,
                aspectRatio: 3, // see: https://fullcalendar.io/docs/aspectRatio

                views: {
                    dayGridMonth: { buttonText: "month" },
                    timeGridWeek: { buttonText: "week" },
                    timeGridDay: { buttonText: "day" },
                    listDay: { buttonText: "list" },
                    listWeek: { buttonText: "list" },
                },

                defaultView: "dayGridMonth",
                defaultDate: TODAY,

                editable: true,
                selectable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: url_loadSchedule,

                select: function (res) {
                    startDate = moment(res.startStr).format("YYYY-MM-DD");
                    endDate = moment(res.endStr).format("YYYY-MM-DD");
                    type = "create";

                    $("#modal_title").text("Add Event");
                    $("#edit_modal_event").modal("show");
                },

                eventClick: function (res) {
                    console.log(res.event.id);
                    startDate = moment(res.event.start).format("YYYY-MM-DD");
                    endDate = moment(res.event.end).format("YYYY-MM-DD");
                    id = res.event.id;

                    loadViewData(res);
                    loadEditData(res);
                },

                eventDrop: function (res) {
                    updateEvent(res);
                },

                eventResize: function (res) {
                    updateEvent(res);
                },

                eventRender: function (info) {
                    var element = $(info.el);

                    if (
                        info.event.extendedProps &&
                        info.event.extendedProps.description
                    ) {
                        if (element.hasClass("fc-day-grid-event")) {
                            element.data(
                                "content",
                                info.event.extendedProps.description
                            );
                            element.data("placement", "top");
                            // KTApp.initPopover(element);
                        } else if (element.hasClass("fc-time-grid-event")) {
                            element
                                .find(".fc-title")
                                .append(
                                    '<div class="fc-description">' +
                                        info.event.extendedProps.description +
                                        "</div>"
                                );
                        } else if (
                            element.find(".fc-list-item-title").lenght !== 0
                        ) {
                            element
                                .find(".fc-list-item-title")
                                .append(
                                    '<div class="fc-description">' +
                                        info.event.extendedProps.description +
                                        "</div>"
                                );
                        }
                    }
                },
            });

            calendar.render();

            const form = document.getElementById("form_event");
            var validator = FormValidation.formValidation(form, {
                fields: {
                    calendar_event_name: {
                        validators: {
                            notEmpty: {
                                message: "Event Name Is Required",
                            },
                        },
                    },
                    calendar_event_description: {
                        validators: {
                            notEmpty: {
                                message: "Event Description Is Required",
                            },
                        },
                    },
                    calendar_event_name: {
                        validators: {
                            notEmpty: {
                                message: "Event Name Is Required",
                            },
                        },
                    },
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: "",
                    }),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            });

            function loadViewData(res) {
                $("#view_modal_event").modal("show");
                var html_event = "<h4> &emsp;" + res.event.title + "</h4>";
                var html_description =
                    "&emsp; &nbsp;" + res.event.extendedProps.description + "";

                $('[data-kt-calendar="event_name"]').html(html_event);
                $('[data-kt-calendar="event_description"]').html(
                    html_description
                );

                if (res.event.allDay == true) {
                    $('[data-kt-calendar="event_start_date"]').text(
                        moment(res.event.start).format("Do MMM, YYYY")
                    );
                    $('[data-kt-calendar="event_end_date"]').text(
                        moment(res.event.end).format("Do MMM, YYYY")
                    );
                } else {
                    $('[data-kt-calendar="event_start_date"]').text(
                        moment(res.event.start).format("Do MMM, YYYY - h:mm a")
                    );
                    $('[data-kt-calendar="event_end_date"]').text(
                        moment(res.event.end).format("Do MMM, YYYY - h:mm a")
                    );
                }
            }

            function loadEditData(res) {
                type = "edit";

                $("#modal_title").text("Edit Event");
                $("#btn_event_submit").html("Edit");

                $("#calendar_event_name").val(res.event.title);
                $("#calendar_event_description").val(
                    res.event.extendedProps.description
                );
                if (res.event.allDay == true) {
                    $("#kt_calendar_datepicker_allday").attr("checked", true);
                    $("#time_row").hide();
                }
                $("#startTime").val(moment(res.event.start).format("HH:mm:ss"));
                $("#endTime").val(moment(res.event.end).format("HH:mm:ss"));
            }

            function updateEvent(res) {
                startDate = moment(res.event.start).format("YYYY-MM-DD");
                endDate = moment(res.event.end).format("YYYY-MM-DD");

                var startTime = moment(res.event.start, "HH:mm a").format(
                    "HH:mm:ss"
                );
                var endTime = moment(res.event.end, "HH:mm a").format(
                    "HH:mm:ss"
                );
                if (res.event.allDay == true) {
                    var startDateTime = startDate;
                    var endDateTime = endDate;
                    className = "fc-event-light fc-event-solid-primary";
                } else {
                    var startDateTime = startDate + "T" + startTime;
                    var endDateTime = startDate + "T" + endTime;
                    className = "fc-event-success";
                }

                type = "edit";
                $.ajax({
                    type: "POST",
                    url: url_eventHandler,
                    cache: false,
                    dataType: "json",
                    data: {
                        _token: $('meta[name="_token"]').attr("content"),
                        id: res.event.id,
                        eventName: res.event.title,
                        eventDescription: res.event.extendedProps.description,
                        startDateTime: startDateTime,
                        endDateTime: endDateTime,
                        className: className,
                        type: type,
                    },
                    success: function (data) {},
                });
            }

            $("#btn_event_submit").click(function (event) {
                event.preventDefault();

                var eventName = $("#calendar_event_name").val();
                var eventDescription = $("#calendar_event_description").val();
                var allDay = $("#kt_calendar_datepicker_allday").is(":checked");
                var startTime = moment($("#startTime").val(), "HH:mm a").format(
                    "HH:mm:ss"
                );
                var endTime = moment($("#endTime").val(), "HH:mm a").format(
                    "HH:mm:ss"
                );

                if (allDay) {
                    var startDateTime = startDate;
                    var endDateTime = endDate;
                    className = "fc-event-light fc-event-solid-primary";
                } else {
                    var startDateTime = startDate + "T" + startTime;
                    var endDateTime = startDate + "T" + endTime;
                    className = "fc-event-success";
                }
                console.log(allDay);
                console.log(startDateTime);
                console.log(endDateTime);

                if (validator) {
                    validator.validate().then(function (status) {
                        if (status == "Valid") {
                            $.ajax({
                                type: "POST",
                                url: url_eventHandler,
                                cache: false,
                                dataType: "json",
                                data: {
                                    _token: $('meta[name="_token"]').attr(
                                        "content"
                                    ),
                                    id: id,
                                    eventName: eventName,
                                    eventDescription: eventDescription,
                                    startDateTime: startDateTime,
                                    endDateTime: endDateTime,
                                    className: className,
                                    type: type,
                                },
                                success: function (data) {
                                    $("#edit_modal_event").modal("hide");
                                    document
                                        .getElementById("form_event")
                                        .reset();
                                    $("#time_row").show();

                                    if (type == "edit") {
                                        var event = calendar.getEventById(id);
                                        event.remove();

                                        calendar.addEvent({
                                            id: data.id,
                                            title: data.title,
                                            start: data.start,
                                            end: data.end,
                                            description: data.description,
                                            className: data.className,
                                        });
                                    } else if (type == "create") {
                                        calendar.addEvent({
                                            id: data.id,
                                            title: data.title,
                                            start: data.start,
                                            end: data.end,
                                            description: data.description,
                                            className: data.className,
                                        });
                                    }
                                    calendar.render();
                                },
                            });
                        } else {
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                },
                            });
                        }
                    });
                }
            });

            $("#btn_cancel, #btn_close, #btn_close_details").click(function (
                event
            ) {
                document.getElementById("form_event").reset();
                validator.resetForm(true);
                $("#kt_calendar_datepicker_allday").attr("checked", false);
                $("#time_row").show();
                $("#modal_view_event").show();
            });

            $("#btn_event_edit").click(function (event) {
                $("#edit_modal_event").modal("show");
            });

            $("#btn_event_delete").click(function (event) {
                Swal.fire({
                    text: "Are you sure you would like to delete this event?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light",
                    },
                }).then(function (result) {
                    if (result.value) {
                        $("#view_modal_event").modal("hide");
                        calendar.getEventById(id).remove();
                        calendar.render();
                        type = "delete";

                        $.ajax({
                            type: "POST",
                            url: url_eventHandler,
                            cache: false,
                            dataType: "json",
                            data: {
                                _token: $('meta[name="_token"]').attr(
                                    "content"
                                ),
                                id: id,
                                type: type,
                            },
                            success: function (data) {
                                console.log(data);
                            },
                        });
                    } else if (result.dismiss === "cancel") {
                        Swal.fire({
                            text: "Your event was not deleted!.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            });

            $("#kt_calendar_datepicker_allday").change(function () {
                if (this.checked) {
                    $("#time_row").hide();
                    $("#startTime").val("");
                    $("#endTime").val("");
                } else {
                    $("#time_row").show();
                }
            });
        },
    };
})();

jQuery(document).ready(function () {
    KTCalendarListView.init();
});
