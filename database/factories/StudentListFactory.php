<?php

namespace Database\Factories;

use App\Models\studentList;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentListFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = studentList::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
