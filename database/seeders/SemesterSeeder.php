<?php

namespace Database\Seeders;

use App\Models\Semester;
use Illuminate\Database\Seeder;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // dd(school::where('code', 'SCCM')->first()->id);


        $datas = [

            ['code' => 'Sept 2021',  'isCurrent' => true],
            ['code' => 'Jan 2022',  'isCurrent' => false],

        ];


        // dd($datas[0]['code']);

        // programme::insert($data);

        foreach ($datas as $data) {
            $checkSemester = Semester::where('code', $data['code'])->first();
            if (!$checkSemester) {
                $storeSemester = new Semester;
                $storeSemester->code = $data['code'];
                $storeSemester->isCurrent = $data['isCurrent'];
                $storeSemester->save();
            }
        }
    }
}
