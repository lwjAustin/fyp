<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Programme;
use App\Models\School;

class ProgrammeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // dd(school::where('code', 'SCCM')->first()->id);


        $datas = [

            ['school_id' => School::where('code', 'SCCA')->first()->id, 'name' => 'Communications & Media', 'code' => 'UCOMM', 'weeks' => '16'],
            ['school_id' => School::where('code', 'SCCA')->first()->id, 'name' => 'Entertainemtnet Arts', 'code' => 'UBEEE', 'weeks' => '16'],

            ['school_id' => School::where('code', 'SCCM')->first()->id, 'name' => 'Computer Science', 'code' => 'UCOMS', 'weeks' => '16'],
            ['school_id' => School::where('code', 'SCCM')->first()->id, 'name' => 'Software Engineering', 'code' => 'USENG', 'weeks' => '16'],

            ['school_id' => School::where('code', 'SOB')->first()->id, 'name' => 'Business Managmenet', 'code' => 'UBCI', 'weeks' => '16'],
            ['school_id' => School::where('code', 'SOB')->first()->id, 'name' => 'Accounting & Finance', 'code' => 'UBUSS', 'weeks' => '16'],

        ];


        // dd($datas[0]['school_id']);

        // programme::insert($data);

        foreach ($datas as $data) {
            $checkProgramme = Programme::where('code', $data['code'])->first();
            if (!$checkProgramme) {
                $storeProgramme = new Programme;
                $storeProgramme->school_id = $data['school_id'];
                $storeProgramme->name = $data['name'];
                $storeProgramme->code = $data['code'];
                $storeProgramme->weeks = $data['weeks'];
                $storeProgramme->save();
            }
        }
    }
}
