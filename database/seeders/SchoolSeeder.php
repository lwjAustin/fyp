<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $datas = [

            ['code' => 'SCCA', 'name' => 'School of Communications and Creative Arts'],
            ['code' => 'SCCM', 'name' => 'School of Computing and Creative Media'],
            ['code' => 'SOB', 'name' => 'School of Business'],
        ];

        // School::insert($data);



        foreach ($datas as $data) {
            $checkSchool = School::where('code', $data['code'])->first();
            if (!$checkSchool) {
                $storeSchool = new School;
                $storeSchool->code = $data['code'];
                $storeSchool->name = $data['name'];

                $storeSchool->save();
            }
        }
    }
}
