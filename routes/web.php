<?php

use App\Models\studentList;
use Database\Seeders\SchoolSeeder;
use Database\Seeders\ProgrammeSeeder;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\DataCollection;
use App\Http\Controllers\StudentListController;
use App\Http\Controllers\Teacher\ScheduleController;
use App\Http\Controllers\Student\CompanyRegistrationController;
use App\Http\Controllers\Teacher\ImportCSVController;
use App\Http\Controllers\Teacher\CompanyListController;
use App\Http\Controllers\Teacher\AnnouncementController;
use App\Http\Controllers\Teacher\ApplicantListController;
use App\Http\Controllers\Student\DashboardStudentController;
use App\Http\Controllers\Student\ProfileStudentController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Student\LogbookController;
use App\Http\Controllers\Teacher\DashboardSupervisorController;
use App\Http\Controllers\Teacher\DashboardCoordinatorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', [AuthenticatedSessionController::class, 'destroy'])->name('test');


Route::group(['middleware' => ['auth', 'role:student']], function () {
    Route::get('/student/dashboard', [DashboardStudentController::class, 'index'])->name('dashboard_student');
    Route::get('/student/companyRegistration', [CompanyRegistrationController::class, 'index'])->name('companyRegistration_page');
    Route::post('/student/saveCompany', [CompanyRegistrationController::class, 'saveCompany'])->name('saveCompany');

    Route::get('/student/profile', [ProfileStudentController::class, 'indexStudent'])->name('profile_student');
    Route::post('/student/editProfile', [ProfileStudentController::class, 'editProfile_student'])->name('editProfile_student');

    Route::get('/student/logbook', [LogbookController::class, 'index'])->name('logbook_page');
    Route::post('/student/getLogbook', [LogbookController::class, 'getLogbook'])->name('getLogbook');
    Route::post('/student/createLogbook', [LogbookController::class, 'createLogbook'])->name('createLogbook');
});

Route::group(['middleware' => ['auth', 'role:coordinator']], function () {
    Route::get('/coordinator/dashboard', [DashboardCoordinatorController::class, 'index'])->name('dashboard_coordinator');
    Route::get('/coordinator/changeSemester/{id}', [DashboardCoordinatorController::class, 'changeSemester'])->name('changeSemester');

    Route::get('/coordinator/importCSV', [ImportCSVController::class, 'index'])->name('importCSV_page');
    Route::post('/coordinator/import/studentList', [ImportCSVController::class, 'importStudentList'])->name('import_studentList');
    Route::post('/coordinator/import/companyList', [ImportCSVController::class, 'importCompanyList'])->name('import_companyList');
    Route::get('/coordinator/download/companyCSV', [ImportCSVController::class, 'downloadCompany'])->name('download_companyCSV');
    Route::get('/coordinator/download/studentCSV', [ImportCSVController::class, 'downloadStudent'])->name('download_studentCSV');

    Route::get('/coordinator/studentList', [StudentListController::class, 'index'])->name('studentList_page');
    Route::get('/coordinator/studentList/api', [StudentListController::class, 'getStudentList'])->name('getStudentList');
    Route::post('/coordinator/studentList/assignSupervisor', [StudentListController::class, 'assignSupervisor'])->name('assignSupervisor');
    Route::post('/coordinator/studentList/deleteStudent', [StudentListController::class, 'deleteStudent'])->name('deleteStudent');

    Route::get('/coordinator/companyList', [CompanyListController::class, 'index'])->name('companyList_page');
    Route::get('/coordinator/companyList/api', [CompanyListController::class, 'getCompanyList'])->name('getCompanyList');
    Route::post('/coordinator/companyList/showDetails', [CompanyListController::class, 'showDetails'])->name('showDetails');
    Route::post('/coordinator/companyList/updateDetails', [CompanyListController::class, 'updateDetails'])->name('updateDetails');
    Route::post('/coordinator/companyList/deleteDetails', [CompanyListController::class, 'deleteDetails'])->name('deleteDetails');

    Route::get('/coordinator/applicantList', [ApplicantListController::class, 'index'])->name('applicantList_page');
    Route::get('/coordinator/applicantList/api', [ApplicantListController::class, 'getApplicantList'])->name('getApplicantList');
    Route::post('/coordinator/applicantList/getApplicantData', [ApplicantListController::class, 'getApplicantData'])->name('getApplicantData');
    Route::post('/coordinator/applicantList/actionApplicant', [ApplicantListController::class, 'actionApplicant'])->name('actionApplicant');
    Route::get('/coordinator/applicantList/logbook/{id}', [ApplicantListController::class, 'indexLogbook'])->name('logbookCoordinator_page');

    Route::get('/coordinator/schedule', [ScheduleController::class, 'index'])->name('schedule_page');
    Route::get('/coordinator/schedule/loadSchedule', [ScheduleController::class, 'loadSchedule'])->name('loadSchedule');
    Route::post('/coordinator/schedule/eventHandler', [ScheduleController::class, 'eventHandler'])->name('eventHandler');

    Route::get('/coordinator/announcements', [AnnouncementController::class, 'index'])->name('announcements_page');
    Route::post('/coordinator/createAnnounncement', [AnnouncementController::class, 'createAnnounncement'])->name('createAnnounncement');
    Route::post('/coordinator/getAnnouncement', [AnnouncementController::class, 'getAnnouncement'])->name('getAnnouncement');
});

Route::group(['middleware' => ['auth', 'role:supervisor']], function () {
    Route::get('/supervisor/dashboard', [DashboardSupervisorController::class, 'index'])->name('dashboard_supervisor');
    Route::get('/supervisor/studentProfile/{id}', [ProfileStudentController::class, 'indexSupervisor'])->name('studentProfile_page');

    // Route::get('/supervisor/profile', [ProfileStudentController::class, 'index'])->name('profile_supervisor');
});



require __DIR__ . '/auth.php';
