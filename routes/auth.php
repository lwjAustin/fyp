<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;



// **********************************************************************************


Route::get('/register/student', [RegisteredUserController::class, 'createStudent'])
    ->middleware('guest')
    ->name('register_student_page');

Route::post('/register/student', [RegisteredUserController::class, 'storeStudent'])
    ->middleware('guest')
    ->name('register_student');

Route::get('/register/teacher', [RegisteredUserController::class, 'createTeacher'])
    ->middleware('guest')
    ->name('register_teacher_page');

Route::post('/register/teacher', [RegisteredUserController::class, 'storeTeacher'])
    ->middleware('guest')
    ->name('register_teacher');



Route::post('/register/showProgramme', [RegisteredUserController::class, 'showProgramme'])
    ->middleware('guest')->name('showProgramme');

Route::post('/register/verifyEmail', [RegisteredUserController::class, 'verifyEmail'])
    ->middleware('guest')->name('verifyEmail');


// **********************************************************************************


Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->middleware('guest')
    ->name('login');

Route::post('/login/student', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest')
    ->name('login_student');

Route::post('/login/teacher', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest')
    ->name('login_teacher');


// **********************************************************************************


Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
    ->middleware('guest')
    ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
    ->middleware('guest')
    ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
    ->middleware('guest')
    ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
    ->middleware('guest')
    ->name('password.update');


// **********************************************************************************


Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
    ->middleware('auth')
    ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
    ->middleware(['auth', 'signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
    ->middleware(['auth', 'throttle:6,1'])
    ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
    ->middleware('auth')
    ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
    ->middleware('auth');


// **********************************************************************************


Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');
