<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class studentList extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'studentID',
        'name',
        'school_id',
        'semester_id',
        'reg_status',
        'IC',
        'supervisor',
        'programme'
    ];
}
