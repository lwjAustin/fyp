<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'student_id',
        'organisation_name',
        'email',
        'contact_no',
        'address',
        'site_supervisor',
        'designation',
        'intern_designation',
        'start_internship',
        'end_internship',
        'job_desc',
        'pdf_path',
        'allowance'
    ];

    public function Student()
    {
        return $this->belongsTo(Student::class);
    }
}
