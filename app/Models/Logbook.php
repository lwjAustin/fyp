<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logbook extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'student_id',
        'week',
        'task',
        'work_done',
        'record_disc',
        'problems',
        'notes'
    ];
}
