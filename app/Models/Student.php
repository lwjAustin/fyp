<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'user_id',
        'school_id',
        'programme_id',
        'teacher_id',
        'studentID',
        'IC',
        'name',
        'profile_path',
        'semester_id',
        'programme',
        'phoneNum',
        'address',
        'app_status',
        'review_status',
        'status'
    ];

    public function Programme()
    {
        return $this->belongsTo(Programme::class);
    }

    public function Teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }


    public function Company()
    {
        return $this->hasOne(Company::class);
    }
}
