<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'user_id',
        'school_id',
        'role'
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Student()
    {
        return $this->hasMany(Student::class);
    }
}
