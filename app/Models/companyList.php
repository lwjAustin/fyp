<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class companyList extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'company',
        'person_incharge',
        'school_id',
        'email',
        'position',
        'contact_no',
        'address',
        'level'
    ];
}
