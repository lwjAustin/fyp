<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'school_id',
        'name',
        'weeks',
        'code'
    ];


    public function Student()
    {
        return $this->hasMany(Student::class);
    }
}
