<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Semester;
use App\Models\studentList;
use Illuminate\Http\Request;
use App\Http\Resources\DataCollection;

class StudentListController extends Controller
{

    public function index()
    {
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;

        $studentLists = studentList::where('school_id', session()->get('school_id'))
            ->where('semester_id', session()->get('semester_id'))
            ->select('programme')
            ->distinct()->get();

        $supervisors = Teacher::where('school_id', session()->get('school_id'))->get();
        // dd($supervisors[0]->user->name);
        // dd($studentLists[0]->programme->code);
        return view('teacher.studentList', compact('studentLists', 'supervisors', 'semester', 'semester_code'));
    }

    public function getStudentList(Request $request)
    {
        // dd($request);
        // $data = studentList::paginate(5, ['*'], 'page', 2);


        // Define the page and number of items per page
        $param_currentPage = 1;
        $param_perPage = 10;

        // Define the default order
        $param_field = 'name';
        $param_sort = 'asc';

        // Get the request parameters
        $params = $request->all();
        $query = studentList::query()->where('school_id', session()->get('school_id'))
            ->where('semester_id', session()->get('semester_id'));

        // Set the current page
        if (isset($params['pagination']['page'])) {
            $param_currentPage = $params['pagination']['page'];
        }

        // Set the number of items
        if (isset($params['pagination']['perpage'])) {
            $param_perPage = $params['pagination']['perpage'];
        }


        // Set the search filter
        if (isset($params['query']['generalSearch'])) {
            $query->where('programme', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('studentID', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('name', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('IC', 'LIKE', "%" . $params['query']['generalSearch'] . "%");
        }

        // Set the programme filter
        if (isset($params['query']['programme'])) {
            if ($params['query']['programme'] != 'all') {

                $query->where('programme', $params['query']['programme']);
            }
        }

        // Set the supervisor filter
        if (isset($params['query']['supervisor'])) {

            if ($params['query']['supervisor'] != 'all') {

                $query->where('supervisor', $params['query']['supervisor']);
            }
        }

        // Set the sort order and field
        if (isset($params['sort']['field'])) {
            $param_field = $params['sort']['field'];
            $param_sort = $params['sort']['sort'];
        }

        // Get how many items there should be
        $total = $query->limit($param_perPage)->count();

        // Get the items defined by the parameters
        $results = $query->skip(($param_currentPage - 1) * $param_perPage)
            ->take($param_perPage)->orderBy($param_field, $param_sort)
            ->get();


        // Create an array with the items and meta data
        $imports = [
            'meta' => [
                "page" => $param_currentPage,
                "pages" => ceil($total / $param_perPage),
                "perpage" => $param_perPage,
                "total" => $total,
                "sort" => $param_sort,
                "field" => $param_field
            ],
            'data' => $results->toArray()
        ];

        return response()->json($imports);
    }

    public function assignSupervisor(Request $request)
    {
        $student_list_ids = $request->get('arr_selected');
        $supervisor = $request->get('selected_supervisor');

        //get teacher _id
        $teacher_id = User::where('name', $supervisor)->first()->teacher->id;

        //loop thru array of selected students
        foreach ($student_list_ids as $student_list_id) {
            // update studentlist table
            $update_student_list = studentList::where('id', $student_list_id)->first();
            $update_student_list->supervisor = $supervisor;
            $update_student_list->save();

            //check if student is registered, if so then save
            $update_student = student::where('studentID', $update_student_list->studentID)->first();
            if ($update_student) {
                $update_student->teacher_id = $teacher_id;
                $update_student->save();
            }
        }
    }

    public function deleteStudent(Request $request)
    {
        $student_list_ids = $request->get('arr_selected');

        foreach ($student_list_ids as $student_list_id) {

            // delete record from studentList table
            $update_student_list = studentList::where('id', $student_list_id)->first();
            $update_student_list->delete();

            // delete record from User and students table
            $update_student = student::where('studentID', $update_student_list->studentID)->first();
            if ($update_student) {
                User::where('id', $update_student->user_id)->first()->delete();
            }
        }
    }
}
