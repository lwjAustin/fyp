<?php

namespace App\Http\Controllers\Student;

use App\Models\Student;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement;
use Illuminate\Mail\Markdown;

class CompanyRegistrationController extends Controller
{
    public function index()
    {

        $isCompany = Company::where('student_id', session()->get('student_id'))->first();

        if ($isCompany) {
            $isPDF = Company::where('student_id', session()->get('student_id'))->first()->pdf_path;
        } else {
            $isPDF = null;
        }

        return view('student.registration', compact('isCompany', 'isPDF'));
        // return view('welcome');
    }

    public function saveCompany(Request $request)
    {
        // dd($request->input_org);
        $company = Company::where('student_id', session()->get('student_id'))->first();
        if (!$company) {
            $company = new Company;
            $company->student_id = session()->get('student_id');

            $student = Student::where('id', session()->get('student_id'))->first();
            $student->app_status = 'submitted';
            $student->save();
        }


        if ($request->file()) {

            $path = public_path('/offer_letters');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $file = $request->file('file')[0];
            $name = uniqid() . '_' . trim($file->getClientOriginalName());
            $file->move($path, $name);


            //delete old file
            if ($company->pdf_path) {
                unlink(public_path('/offer_letters/' . $company->pdf_path));
            }
            $company->pdf_path = $name;
        }

        $company->organisation_name = $request->input_org;
        $company->email = $request->input_email;
        $company->contact_no = $request->input_contact;
        $company->address = $request->input_address;
        $company->site_supervisor = $request->input_supervisor;
        $company->designation = $request->input_designation;
        $company->intern_designation = $request->input_internDesignation;
        $company->start_internship = $request->input_start;
        $company->end_internship = $request->input_end;
        $company->allowance = $request->input_allowance;
        $company->job_desc = $request->input_jobDesc;
        $company->save();

        return redirect()->route('dashboard_student');
    }
}
