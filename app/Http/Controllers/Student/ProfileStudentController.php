<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\companyList;
use App\Models\Semester;
use App\Models\Student;
use App\Models\User;
use Spatie\Geocoder\Facades\Geocoder;

class ProfileStudentController extends Controller
{

    public function indexStudent()
    {

        return view('student.profileStudent');
    }

    public function indexSupervisor($id)
    {


        $student = Student::where('id', $id)->first();
        $company = Company::where('student_id', $id)->first();

        if ($company) {

            $coordinate = Geocoder::setApiKey('AIzaSyDXf5G5L1zomTZXwpt_kogrSUl93xCkozo')
                ->getCoordinatesForAddress($company->address);
        } else {
            $coordinate = null;
        }

        // dd($coordinate['lat']);
        return view('teacher.profileStudent', compact('student', 'company', 'coordinate'));
    }

    public function editProfile_student(Request $request)
    {
        // dd($request->all());
        $editStudent = Student::where('id', session()->get('student_id'))->first();
        $request->validate(
            [
                'input_email' => 'unique:users,email,' . auth()->user()->id
            ],
            [
                // custom message
                'input_email.unique' => 'Email is Taken'
            ]
        );

        //save password
        if ($request->input_password) {
            $request->validate([
                'input_password' => 'required|string|min:8',
            ]);

            $user = User::where('id', auth()->user()->id)
                ->update(['password' => Hash::make($request->input_password)]);
        }


        //save profile picture
        if ($request->file()) {
            # code...
            $path = public_path('/profileImages');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $file = $request->file()['profile_avatar'];
            $name = uniqid() . '_' . trim($file->getClientOriginalName());
            $file->move($path, $name);

            //delete old file
            if ($editStudent->profile_path) {
                unlink(public_path('profileImages/' . $editStudent->profile_path));
            }
            // dd('sdf');
            $editStudent->profile_path = $name;
        } else if (!$request->file()) {
            if ($editStudent->profile_path) {
                unlink(public_path('profileImages/' . $editStudent->profile_path));
            }
            $editStudent->profile_path = null;
        }



        $editUser =  user::where('id', auth()->user()->id)->update(['email' => $request->input_email]);
        $editStudent->phoneNum = $request->input_phoneNum;
        $editStudent->address = $request->input_address;
        $editStudent->save();

        return redirect()->route('dashboard_student');
    }
}
