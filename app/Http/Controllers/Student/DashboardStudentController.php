<?php

namespace App\Http\Controllers\Student;

use App\Models\Logbook;
use App\Models\Student;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Mail\Markdown;
use App\Http\Controllers\Controller;

class DashboardStudentController extends Controller
{
    public function index()
    {
        $announcements = Announcement::orderBy('updated_at', 'DESC')->get();
        $latestWeek = Logbook::where('student_id', session()->get('student_id'))->max('week');
        // if (count($announcements) == 0) {
        //     # code...
        //     dd($announcements);
        // } else {
        //     dd('sdf');
        // }

        return view('student.dashboard_student', compact('announcements', 'latestWeek'));
        // return view('welcome');
    }
}
