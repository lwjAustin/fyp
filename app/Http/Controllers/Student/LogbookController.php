<?php


namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Logbook;
use App\Models\Student;
use Illuminate\Http\Request;

class LogbookController extends Controller
{

    public function index()
    {
        $totalWeeks = Student::where('id', session()->get('student_id'))->first()->Programme->weeks;
        $latestWeek = Logbook::where('student_id', session()->get('student_id'))->max('week');
        // dd($latestWeek);

        if ($totalWeeks == $latestWeek) {
            $isComplete = true;
        } else {
            $isComplete = false;
        }
        // dd($isComplete);
        $logbooks = Logbook::where('student_id', session()->get('student_id'))->orderBy('week', 'DESC')->get();

        return view('student.logbook', compact('logbooks', 'latestWeek', 'isComplete'));
    }

    public function getLogbook(Request $request)
    {
        $logbooks = Logbook::where('id', $request->row_id)->first();
        return response()->json($logbooks);
    }


    public function createLogbook(Request $request)
    {
        // dd($request->all());

        if ($request->get('type') == 'edit') {

            $editLogbook = Logbook::where('id', $request->get('row_id'))->first();
            $editLogbook->task = $request->edit_task;
            $editLogbook->work_done = $request->edit_work_done;
            $editLogbook->record_disc = $request->edit_record_disc;
            $editLogbook->problems = $request->edit_problems;
            $editLogbook->notes = $request->edit_notes;
            $editLogbook->save();

            return response()->json('success');
        } else {

            $latestWeek = Logbook::where('student_id', session()->get('student_id'))->max('week');

            if ($latestWeek) {
                $week = $latestWeek + 1;
            } else {
                $week = 1;
            }

            $newLogbook = new Logbook;
            $newLogbook->student_id = session()->get('student_id');
            $newLogbook->week = $week;
            $newLogbook->task = $request->input_task;
            $newLogbook->work_done = $request->input_work_done;
            $newLogbook->record_disc = $request->input_record_disc;
            $newLogbook->problems = $request->input_problems;
            $newLogbook->notes = $request->input_notes;
            $newLogbook->save();

            return redirect()->back();
        }
    }
}
