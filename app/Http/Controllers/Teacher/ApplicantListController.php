<?php

namespace App\Http\Controllers\Teacher;


use App\Models\User;
use App\Models\Company;
use App\Models\Logbook;
use App\Models\Student;
use App\Models\Semester;
use App\Models\Programme;
use App\Models\companyList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Geocoder\Facades\Geocoder;


class ApplicantListController extends Controller
{
    public function index()
    {
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;


        $students = Student::where('school_id', session()->get('school_id'))
            ->where('semester_id', session()->get('semester_id'))
            ->select('programme')
            ->distinct()->get();

        // dd($students[0]);
        return view('teacher.applicantList', compact('students', 'semester', 'semester_code'));
    }


    public function indexLogbook($id)
    {

        $student = Student::where('id', $id)->first();
        $logbooks = Logbook::where('student_id', $id)->orderBy('week', 'DESC')->get();
        if (count($logbooks) == 0) {

            $logbooks = 'empty';
        }

        return view('teacher.logbookCoordinator', compact('student', 'logbooks'));
    }

    public function getApplicantList(Request $request)
    {
        // dd($request);
        // $data = studentList::paginate(5, ['*'], 'page', 2);


        // Define the page and number of items per page
        $param_currentPage = 1;
        $param_perPage = 10;

        // Define the default order
        $param_field = 'studentID';
        $param_sort = 'asc';

        // Get the request parameters
        $params = $request->all();
        $query = Student::query()->where('school_id', session()->get('school_id'))
            ->where('semester_id', session()->get('semester_id'));

        // Set the current page
        if (isset($params['pagination']['page'])) {
            $param_currentPage = $params['pagination']['page'];
        }

        // Set the number of items
        if (isset($params['pagination']['perpage'])) {
            $param_perPage = $params['pagination']['perpage'];
        }


        // Set the search filter
        if (isset($params['query']['generalSearch'])) {
            $query->where('studentID', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orwhere('name', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orwhere('programme', 'LIKE', "%" . $params['query']['generalSearch'] . "%");
        }

        // set the programme filter
        if (isset($params['query']['programme'])) {
            if ($params['query']['programme'] != 'all') {

                $query->where('programme', $params['query']['programme']);
            }
        }


        // set the supervisor filter
        if (isset($params['query']['supervisor_toggle'])) {
            if ($params['query']['supervisor_toggle'] == 'on') {

                $query->where('teacher_id', session()->get('teacher_id'));
            }
        }

        // Set the sort order and field
        if (isset($params['sort']['field'])) {
            $param_field = $params['sort']['field'];
            $param_sort = $params['sort']['sort'];
        }

        // Get how many items there should be
        $total = $query->limit($param_perPage)->count();

        // Get the items defined by the parameters
        $results = $query->skip(($param_currentPage - 1) * $param_perPage)
            ->take($param_perPage)->orderBy($param_field, $param_sort)
            ->get();



        // Create an array with the items and meta data
        $imports = [
            'meta' => [
                "page" => $param_currentPage,
                "pages" => ceil($total / $param_perPage),
                "perpage" => $param_perPage,
                "total" => $total,
                "sort" => $param_sort,
                "field" => $param_field
            ],
            'data' =>  $this->customData($results)

        ];

        // dd($imports);
        return response()->json($imports);
    }

    public function customData($results)
    {
        // dd($results->toArray());
        $arr_list = [];
        $arr_data = [];

        foreach ($results as $result) {
            $arr_data = [
                "id" => $result->id,
                "studentID" => $result->studentID,
                "name" => $result->user->name,
                "programme" => $result->programme,
                "app_status" => $result->app_status,
                "review_status" => $result->review_status
            ];

            array_push(
                $arr_list,
                $arr_data
            );
        }

        return $arr_list;
    }


    public function getApplicantData(Request $request)
    {
        if ($request->get('type') == 'logbook') {
            $logbooks = Logbook::where('student_id', $request->row_id)->orderBy('week', 'DESC')->get();
            if (count($logbooks) == 0) {

                $logbooks = 'empty';
            }
            return response()->json($logbooks);
        }

        $isNew = false;
        $student = Student::where('id', $request->get('row_id'))->first();
        $company = Company::where('student_id', $request->get('row_id'))->first();
        $checkIsNew = companyList::where('company', $company->organisation_name)->count();
        if ($checkIsNew == 0) {
            $isNew = true;
        }
        if ($request->get('type') == 'map') {
            if ($company) {

                $coordinate = Geocoder::setApiKey('AIzaSyDXf5G5L1zomTZXwpt_kogrSUl93xCkozo')
                    ->getCoordinatesForAddress($company->address);
            } else {
                $coordinate = null;
            }

            return response()->json($coordinate);
        }



        return response()->json(array(

            'company' => $company,
            'student_details' => $student,
            'student_name' => $student->User->name,
            'student_email' => $student->User->email,
            'isNew' => $isNew
        ));
    }

    public function actionApplicant(Request $request)
    {

        if ($request->get('type') == 'review') {
            $student = student::where('id', $request->get('row_id'))->first();
            $student->review_status = 'reviewed';
            $student->save();

            return response()->json('success');
        }
        if ($request->get('type') == 'review save') {
            $student = student::where('id', $request->get('row_id'))->first();
            $student->review_status = 'reviewed';
            $student->save();

            // save  to company list
            $company = Company::where('student_id', $request->get('row_id'))->first();
            $saveCompanyList = new companyList;
            $saveCompanyList->school_id = session()->get('school_id');
            $saveCompanyList->company = $company->organisation_name;
            $saveCompanyList->person_incharge = $company->site_supervisor;
            $saveCompanyList->email = $company->email;
            $saveCompanyList->position = $company->designation;
            $saveCompanyList->contact_no = $company->contact_no;
            $saveCompanyList->address = $company->address;
            $saveCompanyList->level = 'Info not available';
            $saveCompanyList->save();

            // dd($saveCompanyList);

            return response()->json('success');
        }
    }
}
