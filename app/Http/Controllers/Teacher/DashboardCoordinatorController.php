<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Student;
use App\Models\Teacher;
use App\Models\Semester;
use App\Models\Programme;
use App\Models\studentList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardCoordinatorController extends Controller
{

    public function index()
    {
        // dd(session()->all());
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;

        $arr_programmes = $this->displayTiles();
        return view('teacher.dashboard_coordinator', compact('arr_programmes', 'semester', 'semester_code'));
        // return view('layouts.test', compact('arr_programmes'));
    }

    public function displayTiles()
    {
        $arr_programmes = [];
        $programmes = Programme::where('school_id', session()->get('school_id'))->get();

        foreach ($programmes as $programme) {
            $reg_students = Student::where('programme_id', $programme->id)->count();

            if ($reg_students) {
                $reviewed_students = Student::where('programme_id', $programme->id)
                    ->where('review_status', 'reviewed')
                    ->count();
                $app_status_completed = Student::where('programme_id', $programme->id)
                    ->where('app_status', 'submitted')
                    ->count();

                $assigned_students = studentList::where('programme', $programme->code)
                    ->where('supervisor', '!=', 'unassigned')
                    ->count();

                $total_students = studentList::where('programme', $programme->code)->count();

                array_push(
                    $arr_programmes,
                    [
                        'code' => $programme->code,
                        'name' => $programme->name,
                        'reg_students' => $reg_students,
                        'reviewed_students' => $reviewed_students,
                        'app_status_completed' => $app_status_completed,
                        'assigned_students' => $assigned_students,
                        'total_students' => $total_students
                    ]
                );
            }
        }
        // dd($arr_programmes);
        return $arr_programmes;
    }

    public function changeSemester($id)
    {
        // dd(session()->get('semester_id'));

        session(['semester_id' => $id]);
        return redirect()->back();
    }
}
