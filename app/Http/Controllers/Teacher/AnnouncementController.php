<?php

namespace App\Http\Controllers\Teacher;



use Carbon\Carbon;
use App\Models\Semester;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Mail\Markdown;
use App\Http\Controllers\Controller;
use League\HTMLToMarkdown\HtmlConverter;


class AnnouncementController extends Controller
{

    public function index()
    {
        $announcements = Announcement::orderBy('updated_at', 'DESC')->paginate(6);
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;


        return view('teacher.Announcement', compact('announcements', 'semester', 'semester_code'));
    }

    public function createAnnounncement(Request $request)
    {
        // dd($request->all());

        if ($request->get('type') == 'edit') {

            $editAnnouncement = Announcement::where('id', $request->get('row_id'))->first();
            $editAnnouncement->title = $request->edit_title;

            if ($request->get('edit_description') == 'false') {
            } else {
                // dd($request->get('edit_description'));
                $markdown = Markdown::parse($request->get('edit_description'));
                $editAnnouncement->description = nl2br($markdown);
            }

            $editAnnouncement->updated_at = Carbon::now()->setTimezone('Asia/Singapore')->format('Y-m-d H:i:s');
            $editAnnouncement->save();

            return response()->json($editAnnouncement);
        } elseif ($request->get('type') == 'delete') {
            // dd('sdf');
            $deleteAnnouncement = Announcement::where('id', $request->get('row_id'))->first();
            $deleteAnnouncement->delete();
        } else {

            $markdown = Markdown::parse($request->get('input_description'));

            $newAnnouncement = new Announcement;
            $newAnnouncement->teacher_id = session()->get('teacher_id');
            $newAnnouncement->title = $request->input_title;
            $newAnnouncement->description = nl2br($markdown);
            $newAnnouncement->updated_at = Carbon::now()->setTimezone('Asia/Singapore')->format('Y-m-d H:i:s');
            $newAnnouncement->save();

            return redirect()->back();
        }
    }

    public function getAnnouncement(Request $request)
    {
        $announcements = Announcement::where('id', $request->get('row_id'))->first();
        $converter = new HtmlConverter();

        return response()->json(array(
            'title' => $announcements->title,
            'description' => $converter->convert($announcements->description)
        ));
    }
}
