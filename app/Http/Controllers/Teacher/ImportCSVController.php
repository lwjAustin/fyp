<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Semester;
use App\Models\companyList;
use App\Models\studentList;
use Illuminate\Http\Request;
use App\Imports\companyListImport;
use App\Imports\studentListImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;


class ImportCSVController extends Controller
{

    public function index()
    {
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;

        return view('teacher.importCSV', compact('semester', 'semester_code'));
    }


    public function importStudentList(Request $request)
    {
        // dd($request->file());

        $studentList = studentList::where('school_id', session()->get('school_id'));

        if ($studentList) {
            $studentList->delete();
        }

        Excel::import(new studentListImport, request()->file('file'));
    }



    public function importCompanyList(Request $request)
    {

        $companyList = companyList::where('school_id', session()->get('school_id'));
        if ($companyList) {
            $companyList->delete();
        }
        Excel::import(new companyListImport, request()->file('file'));
    }


    public function downloadCompany()
    {
        $file = public_path('excel/COMPANY_LIST.csv');
        $headers = ['Content-Type: application/pdf'];
        $fileName = 'company_list.csv';
        // dd($fileName);

        return response()->download($file, $fileName, $headers);
    }

    public function downloadStudent()
    {

        $file = public_path('excel/STUDENT_LIST.csv');
        $headers = ['Content-Type: application/pdf'];
        $fileName = 'student_list.csv';
        // dd($fileName);

        return response()->download($file, $fileName, $headers);
    }
}
