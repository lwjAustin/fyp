<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\companyList;
use Illuminate\Http\Request;
use App\Models\Semester;

class CompanyListController extends Controller
{
    public function index()
    {
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;

        return view('teacher.companyList', compact('semester', 'semester_code'));
    }

    public function getCompanyList(Request $request)
    {
        // dd($request);
        // $data = studentList::paginate(5, ['*'], 'page', 2);


        // Define the page and number of items per page
        $param_currentPage = 1;
        $param_perPage = 10;

        // Define the default order
        $param_field = 'company';
        $param_sort = 'asc';

        // Get the request parameters
        $params = $request->all();
        $query = companyList::query()->where('school_id', session()->get('school_id'));

        // Set the current page
        if (isset($params['pagination']['page'])) {
            $param_currentPage = $params['pagination']['page'];
        }

        // Set the number of items
        if (isset($params['pagination']['perpage'])) {
            $param_perPage = $params['pagination']['perpage'];
        }


        // Set the search filter
        if (isset($params['query']['generalSearch'])) {
            $query->where('company', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('person_incharge', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('email', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('position', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('contact_no', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('address', 'LIKE', "%" . $params['query']['generalSearch'] . "%")
                ->orWhere('level', 'LIKE', "%" . $params['query']['generalSearch'] . "%");
        }

        // Set the level filter
        if (isset($params['query']['level'])) {

            if ($params['query']['level'] != 'all') {
                if ($params['query']['level'] == 'others') {

                    // dd('df');
                    $query->where('level', 'not like', "%" . 'diploma' . "%")
                        ->where('level', 'not like', "%" . 'degree' . "%");
                } else {

                    // dd('ada');
                    $query->where('level', 'LIKE', "%" . $params['query']['level'] . "%");
                }
            }
        }

        // Set the sort order and field
        if (isset($params['sort']['field'])) {
            $param_field = $params['sort']['field'];
            $param_sort = $params['sort']['sort'];
        }

        // Get how many items there should be
        $total = $query->limit($param_perPage)->count();

        // Get the items defined by the parameters
        $results = $query->skip(($param_currentPage - 1) * $param_perPage)
            ->take($param_perPage)->orderBy($param_field, $param_sort)
            ->get();


        // Create an array with the items and meta data
        $imports = [
            'meta' => [
                "page" => $param_currentPage,
                "pages" => ceil($total / $param_perPage),
                "perpage" => $param_perPage,
                "total" => $total,
                "sort" => $param_sort,
                "field" => $param_field
            ],
            'data' => $results->toArray()
        ];

        return response()->json($imports);
    }

    public function showDetails(Request $request)
    {

        $company_details = companyList::where('id', $request->get('row_id'))->first();

        // dd($company_details);

        return json_encode($company_details);
    }

    public function updateDetails(Request $request)
    {
        // dd($request->all());
        if ($request->get('addNew') == 'true') {
            $update_company = new companyList;
            $update_company->school_id = session()->get('school_id');
            $update_company->company = $request->get('company');
            $update_company->person_incharge = $request->get('person_incharge');
            $update_company->email = $request->get('email');
            $update_company->position = $request->get('position');
            $update_company->contact_no = $request->get('contact_no');
            $update_company->level = $request->get('level');
            $update_company->address = $request->get('address');
        } else {

            $update_company = companyList::where('id', $request->get('row_id'))->first();
            $update_company->company = $request->get('company');
            $update_company->person_incharge = $request->get('person_incharge');
            $update_company->email = $request->get('email');
            $update_company->position = $request->get('position');
            $update_company->contact_no = $request->get('contact_no');
            $update_company->level = $request->get('level');
            $update_company->address = $request->get('address');
        }
        $update_company->save();
    }

    public function deleteDetails(Request $request)
    {
        companyList::where('id', $request->get('row_id'))->first()->delete();
    }

    public function addRecord(Request $request)
    {


        $update_company = new companyList;
        $update_company->company = $request->get('company');
        $update_company->person_incharge = $request->get('person_incharge');
        $update_company->email = $request->get('email');
        $update_company->position = $request->get('position');
        $update_company->contact_no = $request->get('contact_no');
        $update_company->level = $request->get('level');
        $update_company->address = $request->get('address');

        $update_company->save();
    }
}
