<?php

namespace App\Http\Controllers\Teacher;


use App\Models\Schedule;
use App\Models\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    public function index()
    {
        $semester = Semester::all();
        $semester_code = Semester::where('id', session()->get('semester_id'))->first()->code;

        return view('teacher.schedule', compact('semester', 'semester_code'));
    }

    public function loadSchedule(Request $request)
    {
        // dd($request->end);

        $data = Schedule::where('teacher_id', session()->get('teacher_id'))
            ->whereDate('start', '>=', $request->start)
            ->whereDate('end', '<=', $request->end)
            ->get();

        return response()->json($data);
    }

    public function eventHandler(Request $request)
    {
        // dd($request->all());

        if ($request->type == 'create') {
            $createSchedule = new Schedule;
            $createSchedule->teacher_id = session()->get('teacher_id');
            $createSchedule->title = $request->eventName;
            $createSchedule->description = $request->eventDescription;
            $createSchedule->start = $request->startDateTime;
            $createSchedule->end = $request->endDateTime;
            $createSchedule->className = $request->className;
            $createSchedule->save();

            return response()->json($createSchedule);
        } else if ($request->type == 'edit') {
            $editSchedule = Schedule::where('id', $request->id)->first();
            $editSchedule->title = $request->eventName;
            $editSchedule->description = $request->eventDescription;
            $editSchedule->start = $request->startDateTime;
            $editSchedule->end = $request->endDateTime;
            $editSchedule->className = $request->className;
            $editSchedule->save();

            return response()->json($editSchedule);
        } else if ($request->type == 'delete') {
            Schedule::where('id', $request->id)->delete();
        }
    }
}
