<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Student;
use App\Models\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardSupervisorController extends Controller
{
    public function index()
    {

        $students = Student::where('teacher_id', session()->get('teacher_id'))->get();
        // dd($students[0]->Company->organisation_name);

        return view('teacher.dashboard_supervisor', compact('students'));
    }
}
