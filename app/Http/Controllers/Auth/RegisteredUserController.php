<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\School;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Semester;
use App\Models\Programme;
use App\Models\studentList;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function createStudent()
    {
        $schools = School::get();
        return view('auth.register_student', compact('schools'));
    }

    public function createTeacher()
    {
        $schools = School::get();

        return view('auth.register_teacher', compact('schools'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeStudent(Request $request)
    {

        // dd($request->school);
        $request->validate(
            [
                'studentID' => ['required', 'string', 'max:255', 'exists:student_lists,studentID'],
                'school_id' => ['required', 'string', 'max:255'],
                'programme' => ['required', 'string', 'max:255', 'exists:student_lists,programme'],
                'fullname' => ['required', 'string', 'max:255', 'exists:student_lists,name'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phoneNum' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'IC' => ['required', 'string', 'max:255', 'exists:student_lists,IC'],
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ],
            [
                // custom message
                'studentID.exists' => 'Student ID Is Not Enrolled for Internship',
                'IC.exists' => 'IC/Passport Does Not Match the System',
                'programme.exists' => 'Programme Does Not Match the System',
                'fullname.exists' => 'Name Does Not Match the System'
            ]
        );

        $programme = Programme::where('code', $request->programme)->first();
        $semester_id = Semester::where('isCurrent', true)->first()->id;

        //check if studnetID has been assigned supervisor
        $studentList = studentList::where('studentID', $request->studentID)
            ->where('supervisor', '!=', 'unassigned')
            ->first();
        if ($studentList) {
            $teacher_id = User::where('name', $studentList->supervisor)->first()->teacher->id;
        } else {
            $teacher_id = null;
        }

        $user = User::create([
            'name' => $request->fullname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $student = Student::create([
            'user_id' => $user->id,
            'school_id' => $request->school_id,
            'semester_id'     => $semester_id,
            'programme_id' => $programme->id,
            'teacher_id' => $teacher_id,
            'studentID' => $request->studentID,
            'name' => $user->name,
            'programme' => $programme->code,
            'IC' => $request->IC,
            'phoneNum' => $request->phoneNum,
            'address' => $request->address,
            'app_status' => 'pending',
            'review_status' => 'pending'
        ]);

        //update registration status
        $save_reg_status = studentlist::where('studentID', $student->studentID)->first();
        $save_reg_status->reg_status = 'registered';
        $save_reg_status->save();

        //     $check_student = Student::where('studentID', $request->studentID)
        //     ->where('email', $request->email)
        //     ->where('IC', $request->IC)
        //     ->first();

        // if ($check_student) {
        //     $check_student->user_id = $user->id;
        //     $check_student->school_id = $request->school_id;
        //     $check_student->programme_id = $programme_id;
        //     $check_student->studentID = $request->studentID;
        //     $check_student->IC = $request->IC;
        //     $check_student->fullname = $request->fullname;
        //     $check_student->phoneNum = $request->phoneNum;
        //     $check_student->address = $request->address;
        //     $check_student->email = $request->email;
        // }


        event(new Registered($user));
        $user->attachRole('student');
        Auth::login($user);

        //generate sessions var
        session(['school_id' => $request->school_id]);
        session(['programme_id' => $programme->id]);
        session(['student_id' => $student->id]);
        session(['semester_id' => $semester_id]);

        return redirect()->route('dashboard_student');


        // return redirect(RouteServiceProvider::HOME);
    }


    public function storeTeacher(Request $request)
    {

        // dd($request);
        $request->validate(
            [
                'school_id' => ['required', 'string', 'max:255'],
                'role' => [
                    'required',
                    'string',
                    'max:255',
                    Rule::unique('teachers')->where(function ($query) use ($request) {
                        return $query
                            ->where('school_id', $request->school_id)
                            ->where('role', 'coordinator');
                    }),

                ],
                'fullname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ],
            [
                // custom message
                'role.unique' => 'Coordinator Already Exists',
                'email.unique' => 'Email Taken'
            ]
        );


        $user = User::create([
            'name' => $request->fullname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $semester_id = Semester::where('isCurrent', true)->first()->id;

        $teacher = Teacher::create([
            'user_id' => $user->id,
            'school_id' => $request->school_id,
            'role' => $request->role,
        ]);



        session(['school_id' => $request->school_id]);
        session(['teacher_id' => $teacher->id]);
        session(['semester_id' => $semester_id]);


        event(new Registered($user));
        if ($request->role == 'supervisor') {

            $user->attachRole('supervisor');
            Auth::login($user);
            return redirect()->route('dashboard_supervisor');
        } else {

            $user->attachRole('coordinator');
            Auth::login($user);
            return redirect()->route('dashboard_coordinator');
        }

        // return redirect(RouteServiceProvider::HOME);
    }


    public function showProgramme(Request $request)
    {
        $programme_names = Programme::where('school_id', $request->school_id)->get();

        return json_encode($programme_names);
    }


    public function verifyEmail(Request $request)
    {

        $check_Email = User::where('email', $request->get('email'))->first();
        // $check_Email = User::where('email', 'ps@p.com')->first();

        if ($check_Email) {
            return json_encode('not unique');
        } else {
            return json_encode('unique');
        }
    }
}
