<?php

namespace App\Http\Controllers\Auth;

use App\Models\Student;
use App\Models\Teacher;
use App\Models\Semester;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\LoginRequest;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        // dd('asd');
        $request->authenticate();
        $request->session()->regenerate();


        if (Auth::user()->hasRole('student')) {

            $school_id =  Student::where('user_id', Auth::user()->id)->first()->school_id;
            $programme_id =  Student::where('user_id', Auth::user()->id)->first()->programme_id;
            $student_id = Student::where('user_id', Auth::user()->id)->first()->id;


            session(['school_id' => $school_id]);
            session(['programme_id' => $programme_id]);
            session(['student_id' => $student_id]);

            return redirect()->route('dashboard_student');
        } elseif (Auth::user()->hasRole('coordinator')) {

            $school_id =  Teacher::where('user_id', Auth::user()->id)->first()->school_id;
            $teacher_id = Teacher::where('user_id', Auth::user()->id)->first()->id;
            $semester_id = Semester::where('isCurrent', true)->first()->id;


            session(['school_id' => $school_id]);
            session(['teacher_id' => $teacher_id]);
            session(['semester_id' => $semester_id]);

            return redirect()->route('dashboard_coordinator');
        } elseif (Auth::user()->hasRole('supervisor')) {

            $school_id =  Teacher::where('user_id', Auth::user()->id)->first()->school_id;
            $teacher_id = Teacher::where('user_id', Auth::user()->id)->first()->id;

            session(['school_id' => $school_id]);
            session(['teacher_id' => $teacher_id]);
            return redirect()->route('dashboard_supervisor');
        }


        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {

        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('login');
    }
}
