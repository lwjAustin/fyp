<?php

namespace App\Imports;

use App\Models\companyList;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class companyListImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        // dd($row);

        return new companyList([
            'school_id'     => session()->get('school_id'),
            'company'     => $row['company'],
            'person_incharge'    => $row['person_incharge'],
            'email'    => $row['email'],
            'position'    => $row['position'],
            'contact_no'    => $row['contact_no'],
            'address'    => $row['address'],
            'level'    => $row['level'],
        ]);
    }
}
