<?php

namespace App\Imports;

use App\Models\studentList;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Programme;
use App\Models\Semester;
use App\Models\Student;

class studentListImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        // dd($row);
        $check_reg_status = student::where('studentID', $row['student_id'])->first();
        $check_teacher_id = student::where('studentID', $row['student_id'])
            ->where('teacher_id', '!=', NULL)
            ->first();
        $semester_id = Semester::where('isCurrent', true)->first()->id;

        if ($check_teacher_id) {
            $teacher_name = $check_teacher_id->teacher->user->name;
        } else {
            $teacher_name = 'unassigned';
        }



        if ($check_reg_status) {
            return new studentList([
                'school_id'     => session()->get('school_id'),
                'semester_id'     => $semester_id,
                'programme'    => $row['program_enrolled'],
                'studentID'    => $row['student_id'],
                'IC'    => $row['icpassport'],
                'name'    => $row['name'],
                'reg_status'    => 'registered',
                'supervisor'    => $teacher_name,
            ]);
        } else {

            return new studentList([
                'school_id'     => session()->get('school_id'),
                'semester_id'     => $semester_id,
                'programme'    => $row['program_enrolled'],
                'studentID'    => $row['student_id'],
                'IC'    => $row['icpassport'],
                'name'    => $row['name'],
                'reg_status'    => 'pending',
                'supervisor'    => $teacher_name,
            ]);
        }
    }
}
