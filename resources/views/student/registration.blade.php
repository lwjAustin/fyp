@extends('layouts.headerFooter_student')
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Pre-Internship</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Import CSV</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>
                    <!--end::Actions-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left"
                        data-original-title="Quick actions">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="svg-icon svg-icon-success svg-icon-2x">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path
                                            d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path
                                            d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z"
                                            fill="#000000"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover">
                                <li class="navi-header font-weight-bold py-4">
                                    <span class="font-size-lg">Choose Label:</span>
                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"
                                        data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                </li>
                                <li class="navi-separator mb-3 opacity-70"></li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-success">Customer</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-danger">Partner</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-warning">Suplier</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-primary">Member</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-dark">Staff</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-separator mt-3 opacity-70"></li>
                                <li class="navi-footer py-4">
                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                        <i class="ki ki-plus icon-sm"></i>Add new</a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                {{-- ***********************BEGIN FILE DROP***************************** --}}

                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Company Details</h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    @if (!$isCompany)
                        <form id="form_company" method="POST" action="{{ route('saveCompany') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Organisation
                                        Name:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" placeholder="Enter Organisation Name"
                                            name="input_org" id="input_org">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Email:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="email" class="form-control" name="input_email" id="input_email"
                                            placeholder="Enter Email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Contact
                                        Number:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="number" class="form-control" name="input_contact" id="input_contact"
                                            placeholder="Enter Contact No">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Address:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <textarea class="form-control" name="input_address" id="input_address"
                                            placeholder="Enter Address" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Site
                                        Supervisor:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" name="input_supervisor"
                                            id="input_supervisor" placeholder="Enter Site Supervisor">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Designation:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" name="input_designation"
                                            id="input_designation" placeholder="Enter Designation">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Intern's
                                        Designation:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" name="input_internDesignation"
                                            id="input_internDesignation" placeholder="Enter Interh's Designation">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Start
                                        Internship:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" name="input_start" id="input_start"
                                            placeholder="Enter Start Date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">End
                                        Internship:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="text" class="form-control" name="input_end" id="input_end"
                                            placeholder="Enter End Date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Allowance:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <input type="number" class="form-control" name="input_allowance"
                                            id="input_allowance" placeholder="Enter Allowance">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Job Description:
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <textarea class="form-control" name="input_jobDesc" id="input_jobDesc"
                                            placeholder="Required to fill in if you do not obtain an Offer Letter"
                                            rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Upload Offer Letter:</label>
                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                        <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                            <div class="dropzone-panel mb-lg-0 mb-2">
                                                <a class="dropzone-select btn btn-light-primary font-weight-bold btn-sm dz-clickable"
                                                    id="dropzone_offerLetter">Attach
                                                    files</a>
                                                <a class="dropzone-remove-all btn btn-light-primary font-weight-bold btn-sm"
                                                    style="display: none;">Remove</a>
                                            </div>
                                            <div class="dropzone-items">

                                            </div>
                                            <div class="dz-default dz-message"><button class="dz-button"
                                                    type="button">Drop files here to upload</button></div>
                                        </div>
                                        <span class="form-text text-muted">Max file size 2MB & PDF files only</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-9">
                                        <button type="reset" class="btn btn-light-primary mr-2">Cancel</button>
                                        <button type="button" class="btn btn-primary" id="btn_submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @else
                        @include('student.formCompany', [$isCompany])
                    @endif
                </div>

                <div class="row">
                </div>
            </div>
            <!--end::Container-->
        </div>


        <!--end::Entry-->
    </div>


@endsection


@section('script')

    <script>
        $(document).ready(function() {
            //change submit to edit
            var isCompany = '{{ $isCompany }}';
            var isPDF = '{{ $isPDF }}';
            if (isCompany) {
                console.log('sdf');
                $("#btn_submit").html('Edit')
            }

            var uploaded_file = false;
            var id = '#kt_dropzone_4';



            $('#dropzone_offerLetter').dropzone({
                autoProcessQueue: false,
                uploadMultiple: true,
                url: '{{ route('saveCompany') }}', // Set the url for your upload script location
                // paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 2, // MB
                addRemoveLinks: true,
                acceptedFiles: ".pdf",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                previewsContainer: id + " .dropzone-items",
                clickable: id + " .dropzone-select",
                accept: function(file, done) {
                    if (this.files.length == 1) {
                        uploaded_file = true;
                    } else {
                        uploaded_file = false;
                    }
                    done();
                },
                removedfile: function(file) {
                    file.previewElement.remove();
                    if (this.files.length == 1) {
                        uploaded_file = true;
                    } else {
                        uploaded_file = false;
                    }
                    done();
                },
                init: function() {
                    this.hiddenFileInput.removeAttribute('multiple');
                    this.on("maxfilesexceeded", function(file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });

                    //send all the form data along with the files:
                    this.on("sendingmultiple", function(data, xhr, formData) {
                        formData.append("input_org", $("#input_org").val());
                        formData.append("input_email", $("#input_email").val());
                        formData.append("input_contact", $("#input_contact").val());
                        formData.append("input_address", $("#input_address").val());
                        formData.append("input_supervisor", $("#input_supervisor").val());
                        formData.append("input_designation", $("#input_designation").val());
                        formData.append("input_internDesignation", $("#input_internDesignation")
                            .val());
                        formData.append("input_start", $("#input_start").val());
                        formData.append("input_end", $("#input_end").val());
                        formData.append("input_allowance", $("#input_allowance").val());
                        formData.append("input_jobDesc", $("#input_jobDesc").val());
                        // formData.append("lastname", jQuery("#lastname").val());
                    });
                }

            });


            const form = document.getElementById("form_company");
            var validator = FormValidation.formValidation(form, {
                fields: {
                    input_org: {
                        validators: {
                            notEmpty: {
                                message: "Organisation Name Is Required",
                            },
                        },
                    },
                    input_email: {
                        validators: {
                            notEmpty: {
                                message: "Email Is Required",
                            },
                            emailAddress: {
                                message: "The value is not a valid email address",
                            },
                        },
                    },
                    input_contact: {
                        validators: {
                            notEmpty: {
                                message: "Contact Number Is Required",
                            },
                        },
                    },
                    input_address: {
                        validators: {
                            notEmpty: {
                                message: "Address Is Required",
                            },
                        },
                    },
                    input_supervisor: {
                        validators: {
                            notEmpty: {
                                message: "Site Supervisor Is Required",
                            },
                        },
                    },
                    input_designation: {
                        validators: {
                            notEmpty: {
                                message: "Designation Is Required",
                            },
                        },
                    },
                    input_internDesignation: {
                        validators: {
                            notEmpty: {
                                message: "Intern Designation Is Required",
                            },
                        },
                    },
                    input_start: {
                        validators: {
                            notEmpty: {
                                message: "Start Date Is Required",
                            },
                        },
                    },
                    input_end: {
                        validators: {
                            notEmpty: {
                                message: "End Date Is Required",
                            },
                        },
                    },
                    input_allowance: {
                        validators: {
                            notEmpty: {
                                message: "Allowance Is Required",
                            },
                        },
                    },
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        // rowSelector: ".fv-row",
                        // eleInvalidClass: "",
                        // eleValidClass: "",
                    }),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                },
            });


            $("#btn_submit").click(function(event) {
                // event.preventDefault();
                if (validator) {
                    validator.validate().then(function(status) {
                        if (status == "Valid") {

                            if (isPDF) {
                                saveExistingRecord();
                            } else {
                                saveNewRecord();
                            }


                        } else {
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                },
                            });
                        }
                    });
                }



            });


            function saveNewRecord() {
                //must select one for new record
                if (uploaded_file == true || $("#input_jobDesc").val()) {
                    console.log('either one is selected');

                    // has job desc, no file
                    if (uploaded_file == false) {
                        Swal.fire({
                            title: "Success!",
                            text: "Details Was Saved!",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {
                                form.submit();

                            }
                        });

                    } else {
                        //has offer letter, no job desc
                        Dropzone.forElement("#dropzone_offerLetter").processQueue();

                        Swal.fire({
                            title: "Success!",
                            text: "Details Was Saved!",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {
                                window.location.href =
                                    "{{ route('dashboard_student') }}";
                            }
                        });

                    }

                } else {
                    Swal.fire({
                        text: "Please Upload Offer Letter Or Fill In Job Description",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary",
                        },
                    });
                }
            }

            function saveExistingRecord() {
                //must select one for new record
                if (uploaded_file == true) {

                    //has offer letter, no job desc
                    Dropzone.forElement("#dropzone_offerLetter").processQueue();

                    Swal.fire({
                        title: "Success!",
                        text: "Details Was Saved!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            window.location.href =
                                "{{ route('dashboard_student') }}";
                        }
                    });
                } else {
                    Swal.fire({
                        title: "Success!",
                        text: "Details Was Saved!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }).then(function(result) {
                        if (result.value) {
                            form.submit();

                        }
                    });
                }
            }

        });
    </script>

@endsection
