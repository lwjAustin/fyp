@extends('layouts.headerFooter_student')
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none"
                        id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                        <!--end::Title-->
                        <!--begin::Separator-->
                        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                        <!--end::Separator-->
                        <!--begin::Search Form-->
                        <div class="d-flex align-items-center" id="kt_subheader_search">
                            <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">Announcement</span>
                        </div>



                    </div>
                    <!--end::Page Heading-->
                </div>

            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Profile Personal Information-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-300px w-xl-325px" id="kt_profile_aside">
                        <!--begin::Nav Panel Widget 2-->
                        <div class="card card-custom gutter-b" style="margin-bottom: 2px">
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Wrapper-->
                                <div class="d-flex justify-content-between flex-column pt-4 h-100">
                                    <!--begin::Container-->
                                    <div class="pb-5">
                                        <!--begin::Header-->
                                        <div class="d-flex flex-column flex-center">
                                            <!--begin::Symbol-->
                                            <div class="symbol symbol-120 symbol-circle symbol-success overflow-hidden">
                                                <span class="symbol-label">
                                                    @if (auth()->user()->student->profile_path)
                                                        <img src="{{ asset('profileImages/' . auth()->user()->student->profile_path) }}"
                                                            class="h-100 align-self-end" alt="">
                                                    @else
                                                        <img src="#" class="h-75 align-self-end" alt="">
                                                    @endif
                                                </span>
                                            </div>
                                            <!--end::Symbol-->
                                            <!--begin::Username-->
                                            <a href="{{ route('profile_student') }}"
                                                class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">{{ auth()->user()->name }}</a>
                                            <!--end::Username-->
                                            <!--begin::Info-->
                                            <div class="font-weight-bold text-dark-50 font-size-sm pb-6">
                                                {{ auth()->user()->student->programme }}</div>
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Header-->
                                        <!--begin::Body-->
                                        <div class="pt-1">

                                            <div class="d-flex align-items-center pb-9">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-45 symbol-light mr-4">
                                                    <span class="symbol-label">
                                                        <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <path
                                                                        d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z"
                                                                        fill="#000000" fill-rule="nonzero"></path>
                                                                    <circle fill="#000000" opacity="0.3" cx="12" cy="10"
                                                                        r="6"></circle>
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Text-->
                                                <div class="d-flex flex-column flex-grow-1">
                                                    <a href="#"
                                                        class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Company</a>
                                                    <span class="text-muted font-weight-bold">Registration</span>
                                                </div>
                                                <!--end::Text-->
                                                <!--begin::label-->
                                                @if (auth()->user()->student->app_status == 'pending')
                                                    <span
                                                        class="font-weight-bolder label label-xl label-light-danger label-inline px-3 py-5 min-w-45px">pending</span>
                                                @else
                                                    <span
                                                        class="font-weight-bolder label label-xl label-light-success label-inline px-3 py-5 min-w-45px">submitted</span>
                                                @endif


                                                <!--end::label-->
                                            </div>
                                            <div class="d-flex align-items-center pb-9">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-45 symbol-light mr-4">
                                                    <span class="symbol-label">
                                                        <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                                    <path
                                                                        d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path
                                                                        d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                                                        fill="#000000" fill-rule="nonzero" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Text-->
                                                <div class="d-flex flex-column flex-grow-1">
                                                    <a href="#"
                                                        class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Supervisor</a>
                                                    <span class="text-muted font-weight-bold">Assigned</span>
                                                </div>
                                                <!--end::Text-->
                                                <!--begin::label-->
                                                @if (auth()->user()->student->teacher_id)
                                                    <span
                                                        class="font-weight-bolder label label-xl label-light-warning label-inline px-3 py-5 min-w-45px">{{ auth()->user()->student->Teacher->User->name }}</span>
                                                @else
                                                    <span
                                                        class="font-weight-bolder label label-xl label-light-danger label-inline px-3 py-5 min-w-45px">pending</span>
                                                @endif


                                                <!--end::label-->
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-45 symbol-light mr-4">
                                                    <span class="symbol-label">
                                                        <span class="svg-icon svg-icon-2x svg-icon-dark-50">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Home/Globe.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                                        rx="1.5"></rect>
                                                                    <path
                                                                        d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                                        fill="#000000" opacity="0.3"></path>
                                                                </g>
                                                            </svg>

                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Text-->
                                                <div class="d-flex flex-column flex-grow-1">
                                                    <a href="#"
                                                        class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Weekly</a>
                                                    <span class="text-muted font-weight-bold">Logbook</span>
                                                </div>
                                                <!--end::Text-->
                                                <!--begin::label-->
                                                <span
                                                    class="font-weight-bolder label label-xl label-light-info label-inline py-5 min-w-45px">{{ intval($latestWeek) }}/{{ auth()->user()->student->Programme->weeks }}</span>
                                                <!--end::label-->
                                            </div>
                                            <!--end::Item-->
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Nav Panel Widget 2-->
                        <!--begin::List Widget 17-->

                        <!--end::List Widget 17-->
                    </div>
                    <!--end::Aside-->
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="col-lg-12" style="overflow-y:scroll; overflow-x:hidden; height:572px;">

                            @if (count($announcements) >= 1)
                                @foreach ($announcements as $announcement)
                                    <div class="card card-custom">
                                        <div class="card-header">
                                            <div class="card-title">
                                                <span class="card-icon">
                                                    <i class="flaticon2-sheet text-primary"></i>
                                                </span>
                                                <h3 class="card-label">{{ $announcement->title }}</h3>
                                            </div>
                                            <div class="card-toolbar">
                                                <span
                                                    class="text-muted font-weight-bold">{{ Carbon\Carbon::parse($announcement->updated_at)->format('g:i A M d') }}</span>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            {{ Illuminate\Mail\Markdown::parse($announcement->description) }}
                                        </div>
                                    </div>

                                    <div class="example example-compact mt-2 gutter-b">
                                    </div>
                                @endforeach
                            @else
                                <div class="card card-custom">
                                    <div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
                                        <h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!
                                        </h1>
                                        <p class="font-weight-boldest display-4">No Announcements Yet</p>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

@endsection
