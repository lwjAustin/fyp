@extends('layouts.headerFooter_student')

@section('content')


    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Pre-Internship</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Import CSV</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>
                    <!--end::Actions-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left"
                        data-original-title="Quick actions">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="svg-icon svg-icon-success svg-icon-2x">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path
                                            d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path
                                            d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z"
                                            fill="#000000"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover">
                                <li class="navi-header font-weight-bold py-4">
                                    <span class="font-size-lg">Choose Label:</span>
                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"
                                        data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                </li>
                                <li class="navi-separator mb-3 opacity-70"></li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-success">Customer</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-danger">Partner</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-warning">Suplier</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-primary">Member</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-text">
                                            <span class="label label-xl label-inline label-light-dark">Staff</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="navi-separator mt-3 opacity-70"></li>
                                <li class="navi-footer py-4">
                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                        <i class="ki ki-plus icon-sm"></i>Add new</a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                {{-- ***********************BEGIN FILE DROP***************************** --}}

                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    @if ($errors->any())
                        <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert" id="validateAlert">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <x-auth-validation-errors class="alert-text" :errors="$errors" />
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="ki ki-close"></i></span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <!--begin::Form-->
                    <form class="form" method="POST" action="{{ route('editProfile_student') }}"
                        enctype="multipart/form-data" id="form_profile">
                        @csrf
                        <!--begin::Heading-->
                        <div class="form-group row"></div>
                        <div class="form-group row">
                            <div class="col-lg-9 col-xl-6 offset-xl-3">
                                <h3 class="font-size-h6 mb-5">Account Info:</h3>
                            </div>
                        </div>
                        <!--end::Heading-->
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Photo</label>
                            <div class="col-lg-9 col-xl-9">
                                <div class="image-input image-input-outline image-input-circle" id="kt_user_avatar"
                                    style="background-image: url(assets/media/users/blank.png)">
                                    @if (auth()->user()->student->profile_path)
                                        <div class="image-input-wrapper"
                                            style="background-image: url({{ asset('profileImages/' . auth()->user()->student->profile_path) }})">
                                        </div>
                                    @else
                                        <div class="image-input-wrapper" style="background-image: url()">
                                        </div>
                                    @endif
                                    <label
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="change" data-toggle="tooltip" title=""
                                        data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="profile_avatar" accept="image/*">
                                        <input type="hidden" name="profile_avatar_remove">
                                    </label>
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="cancel" data-toggle="tooltip" title=""
                                        data-original-title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="remove" data-toggle="tooltip" title=""
                                        data-original-title="Remove avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Full Name</label>
                            <div class="col-lg-9 col-xl-6">
                                <span class="form-control form-control-lg form-control-solid"
                                    type="text">{{ auth()->user()->name }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Student ID</label>
                            <div class="col-lg-9 col-xl-6">
                                <span class="form-control form-control-lg form-control-solid"
                                    type="text">{{ auth()->user()->student->studentID }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Programme</label>
                            <div class="col-lg-9 col-xl-6">
                                <span class="form-control form-control-lg form-control-solid"
                                    type="text">{{ auth()->user()->student->programme }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">IC/Passport</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg input-group-solid">
                                    <span type="number"
                                        class="form-control form-control-lg form-control-solid">{{ auth()->user()->student->IC }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <!--begin::Heading-->
                        <div class="row">
                            <div class="col-lg-9 col-xl-6 offset-xl-3">
                                <h3 class="font-size-h6 mb-5">Student Info:</h3>
                            </div>
                        </div>
                        <!--end::Heading-->
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">New Password</label>
                            <div class="col-lg-9 col-xl-6">
                                <input class="form-control form-control-lg form-control-solid" type="text"
                                    placeholder="Leave as blank if no changes" name="input_password"></input>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Email</label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="input-group input-group-lg input-group-solid">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-at"></i>
                                        </span>
                                    </div>
                                    <input type="email" class="form-control form-control-lg form-control-solid"
                                        value="{{ auth()->user()->email }}" name="input_email">

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Phone Number</label>
                            <div class="col-lg-9 col-xl-6">
                                <input class="form-control form-control-lg form-control-solid" type="number"
                                    value="{{ auth()->user()->student->phoneNum }}" name="input_phoneNum">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Address</label>
                            <div class="col-lg-9 col-xl-6">
                                <textarea class="form-control form-control-lg form-control-solid" name="input_address"
                                    id="input_address" rows="3">{{ auth()->user()->student->address }}</textarea>
                            </div>
                        </div>

                        <div class="separator separator-dashed my-10"></div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-9">
                                    <button type="reset" class="btn btn-light-primary mr-2">Cancel</button>
                                    <button type="button" class="btn btn-primary" id="btn_edit">Edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                </div>
            </div>
            <!--end::Container-->
        </div>


        <!--end::Entry-->
    </div>


@endsection


@section('script')
    <script src="{{ asset('assets/js/pages/custom/education/student/profile.js') }}"></script>

    <script>
        const form = document.getElementById("form_profile");
        var validator = FormValidation.formValidation(form, {
            fields: {
                input_email: {
                    validators: {
                        notEmpty: {
                            message: "Email Is Required",
                        },
                        emailAddress: {
                            message: "The value is not a valid email address",
                        },
                    },
                },
                input_IC: {
                    validators: {
                        notEmpty: {
                            message: "IC/Passport Is Required",
                        },
                    },
                },
                input_phoneNum: {
                    validators: {
                        notEmpty: {
                            message: "Phone Number Is Required",
                        },
                    },
                },
                input_address: {
                    validators: {
                        notEmpty: {
                            message: "Address Is Required",
                        },
                    },
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    // rowSelector: ".fv-row",
                    eleInvalidClass: "",
                    eleValidClass: "",
                }),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });


        $("#btn_edit").click(function(event) {
            event.preventDefault();
            if (validator) {
                validator.validate().then(function(status) {
                    if (status == "Valid") {
                        console.log('sdf');
                        Swal.fire({
                            text: "Profile Updated",
                            icon: "success",
                            showConfirmButton: false,
                            timer: 1500,
                        });

                        form.submit();

                    } else {
                        Swal.fire({
                            text: "Sorry, looks like there are some errors detected, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            }



        });
    </script>


@endsection
