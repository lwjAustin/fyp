@extends('layouts.headerFooter_student')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Weekly Logbook</h5>
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->

                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Profile Personal Information-->
                <div class="row">
                    <div class="flex-row-fluid">
                        <div class="col-lg-12">
                            <div class="card card-custom card-collapsed" data-card="true" id="kt_card_3">
                                <div class="card-header">
                                    <div class="card-title">

                                        @if ($latestWeek)
                                            <h3 class="card-label">Logbook Week {{ $latestWeek + 1 }}</h3>
                                        @else
                                            <h3 class="card-label">Logbook Week 1</h3>

                                        @endif
                                    </div>
                                    <div class="card-toolbar">
                                        <a href="#" class="btn btn-light-primary font-weight-bold" data-card-tool="toggle"
                                            data-toggle="tooltip" data-placement="top" title="Add New">
                                            <i class="ki ki-plus"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin::Form-->
                                    <form class="form form-label" method="POST" action="{{ route('createLogbook') }}"
                                        id="form-add">
                                        @csrf
                                        <div class="card-body">

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Task:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="input_task" id="input_task"
                                                        placeholder="Enter Task" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Work
                                                    Done:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="input_work_done"
                                                        id="input_work_done" placeholder="Enter Work Done"
                                                        rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Record of
                                                    Discussion:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="input_record_disc"
                                                        id="input_record_disc" placeholder="Enter Record of Discussion"
                                                        rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Problems
                                                    Encountered:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="input_problems"
                                                        id="input_problems" placeholder="Enter Problems Encountered"
                                                        rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Further
                                                    Notes:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="input_notes" id="input_notes"
                                                        placeholder="Enter Futher Notes" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                                    <button type="button" class="btn btn-primary"
                                                        id="btn_add_logbook">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="example example-compact mt-2 gutter-b">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if (count($logbooks) >= 1)
                        @foreach ($logbooks as $logbook)
                            <div class="col-lg-12">
                                <div class="card card-custom" card_row_id="{{ $logbook->id }}">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="flaticon2-sheet text-primary"></i>

                                            </span>
                                            <h3 class="card-label">Logbook Week {{ $logbook->week }}</h3>

                                            <span class="text-muted font-weight-bold">
                                                {{-- <small>{{ Carbon\Carbon::parse($logbook->updated_at)->format('g:i A M d') }}</small> --}}
                                            </span>
                                        </div>
                                        <div class="card-toolbar" modal_row_id="{{ $logbook->id }}">
                                            <a href="#" class="btn btn-icon btn-sm btn-light-success mr-1"
                                                id="btn_show_modal">
                                                <i class="ki ki-bold-sort icon-nm"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="modal fade" id="logbook_modal" data-backdrop="static" tabindex="-1" role="dialog"
                        aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Logbook</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form class="form form-label" method="POST" action="{{ route('createLogbook') }}"
                                        id="form-edit">
                                        @csrf
                                        <div class="card-body">

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Task:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="edit_task" id="edit_task"
                                                        placeholder="Enter Task" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Work
                                                    Done:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="edit_work_done"
                                                        id="edit_work_done" placeholder="Enter Work Done"
                                                        rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Record of
                                                    Discussion:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="edit_record_disc"
                                                        id="edit_record_disc" placeholder="Enter Record of Discussion"
                                                        rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Problems
                                                    Encountered:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="edit_problems" id="edit_problems"
                                                        placeholder="Enter Problems Encountered" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Further
                                                    Notes:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" name="edit_notes" id="edit_notes"
                                                        placeholder="Enter Futher Notes" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <button type="reset" class="btn btn-secondary"
                                                        data-dismiss="modal">Cancel</button>
                                                    <button type="button" class="btn btn-primary"
                                                        id="btn_edit_logbook">Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="container">
            <div class="d-flex justify-content-lg-center">
                {{-- {{ $announcements->links() }} --}}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var isComplete = '{{ $isComplete }}';
        if (isComplete == true) {
            console.log('tru');

            $("#kt_card_3").hide();
        }



        const form_add = document.getElementById("form-add");
        var validator_add = FormValidation.formValidation(form_add, {
            fields: {
                input_task: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                input_work_done: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                input_record_disc: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                input_problems: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    // rowSelector: ".fv-row",
                    // eleInvalidClass: "",
                    // eleValidClass: "",
                }),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });



        $("#btn_add_logbook").click(function(event) {
            event.preventDefault();

            if (validator_add) {
                validator_add.validate().then(function(status) {
                    if (status == "Valid") {

                        Swal.fire({
                            title: "Success!",
                            text: "Announcement Edited",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {
                                console.log('sdfsdf');
                                $("#form-add").submit();
                            }
                        });

                    } else {
                        Swal.fire({
                            text: "Details are missing, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            }

        });



        const form_edit = document.getElementById("form-edit");
        var validator_edit = FormValidation.formValidation(form_edit, {
            fields: {
                edit_task: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                edit_work_done: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                edit_record_disc: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
                edit_problems: {
                    validators: {
                        notEmpty: {
                            message: "Description Is Required",
                        },
                    },
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    // rowSelector: ".fv-row",
                    // eleInvalidClass: "",
                    // eleValidClass: "",
                }),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });



        $("#btn_edit_logbook").click(function(event) {
            event.preventDefault();
            var type = 'edit';
            console.log('hi');

            if (validator_edit) {
                validator_edit.validate().then(function(status) {
                    if (status == "Valid") {
                        $.ajax({
                            type: "POST",
                            url: '{{ route('createLogbook') }}',
                            dataType: "json",
                            data: {
                                _token: $('meta[name="_token"]').attr(
                                    "content"
                                ),
                                type: type,
                                row_id: row_id,
                                edit_task: $("#edit_task").val(),
                                edit_work_done: $("#edit_work_done").val(),
                                edit_record_disc: $("#edit_record_disc").val(),
                                edit_problems: $("#edit_problems").val(),
                                edit_notes: $("#edit_notes").val(),
                            },
                            success: function(res) {
                                console.log(res);
                            },
                        });

                        Swal.fire({
                            title: "Success!",
                            text: "Logbook Edited",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {

                                $("#logbook_modal").modal("hide");
                            }
                        });

                    } else {
                        Swal.fire({
                            text: "Details are missing, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            }

        });



        $(".card-toolbar").on("click", "#btn_show_modal", function() {

            row_id = $(this).parent().attr("modal_row_id");

            console.log(row_id);

            $.ajax({
                type: "POST",
                url: '{{ route('getLogbook') }}',
                dataType: "json",
                data: {
                    _token: $('meta[name="_token"]').attr(
                        "content"
                    ),
                    row_id: row_id,
                },
                success: function(res) {
                    // console.log(res);
                    validator_edit.resetForm(true);

                    $("#edit_task").val(res.task);
                    $("#edit_work_done").val(res.work_done);
                    $("#edit_record_disc").val(res.record_disc);
                    $("#edit_problems").val(res.problems);
                    $("#edit_notes").val(res.notes);

                    $("#logbook_modal").modal("show");

                },
            });
        });
    </script>
@endsection
