<form id="form_company" method="POST" action="{{ route('saveCompany') }}" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Organisation
                Name:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" placeholder="Enter Organisation Name" name="input_org"
                    id="input_org" value="{{ $isCompany->organisation_name }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Email:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="email" class="form-control" name="input_email" id="input_email" placeholder="Enter Email"
                    value="{{ $isCompany->email }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Contact
                Number:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="number" class="form-control" name="input_contact" id="input_contact"
                    placeholder="Enter Contact No" value="{{ $isCompany->contact_no }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Address:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <textarea class="form-control" name="input_address" id="input_address" placeholder="Enter Address"
                    rows="3">{{ $isCompany->address }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Site
                Supervisor:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" name="input_supervisor" id="input_supervisor"
                    placeholder="Enter Site Supervisor" value="{{ $isCompany->site_supervisor }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Designation:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" name="input_designation" id="input_designation"
                    placeholder="Enter Designation" value="{{ $isCompany->designation }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Intern's
                Designation:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" name="input_internDesignation" id="input_internDesignation"
                    placeholder="Enter Interh's Designation" value="{{ $isCompany->intern_designation }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Start
                Internship:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" name="input_start" id="input_start"
                    placeholder="Enter Start Date" value="{{ $isCompany->start_internship }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">End
                Internship:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="text" class="form-control" name="input_end" id="input_end" placeholder="Enter End Date"
                    value="{{ $isCompany->end_internship }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Allowance:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <input type="number" class="form-control" name="input_allowance" id="input_allowance"
                    placeholder="Enter Allowance" value="{{ $isCompany->allowance }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Job Description:
                <span class="text-danger">*</span>
            </label>

            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <textarea class="form-control" name="input_jobDesc" id="input_jobDesc"
                    placeholder="Required to fill in if you do not obtain an Offer Letter"
                    rows="3">{{ $isCompany->job_desc }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label text-right">Upload Offer Letter:</label>
            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                    <div class="dropzone-panel mb-lg-0 mb-2">
                        <a class="dropzone-select btn btn-light-primary font-weight-bold btn-sm dz-clickable"
                            id="dropzone_offerLetter">Attach
                            files</a>
                        <a class="dropzone-remove-all btn btn-light-primary font-weight-bold btn-sm"
                            style="display: none;">Remove</a>
                    </div>
                    <div class="dropzone-items">

                    </div>
                    <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to
                            upload</button></div>
                </div>
                <span class="form-text text-muted">Max file size 1MB & PDF files only</span>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
                <button type="reset" class="btn btn-light-primary mr-2">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_submit">Submit</button>
            </div>
        </div>
    </div>
</form>
