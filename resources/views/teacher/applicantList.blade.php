@extends('layouts.headerFooter_teacher')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Applicant List</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pre-Internship</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Applicant List</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                @include('layouts.semesterDropdown', [$semester_code, $semester])

            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Applicant List
                                <span class="d-block text-muted pt-2 font-size-sm">Review Applicant Data</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <label class="row-3 row-form-label">Superviosr</label>
                            <div class="col-3">
                                <span class="switch switch-icon">
                                    <label>
                                        <input class="form-check-input" type="checkbox" name="select"
                                            id="kt_datatable_supervisor_toggle" />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="Search..."
                                                    id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0 d-none d-md-block">Programme:</label>
                                                <select class="form-control" id="kt_datatable_search_programme">
                                                    <option value="all">All</option>
                                                    @foreach ($students as $student)
                                                        <option value="{{ $student->programme }}">
                                                            {{ $student->programme }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0 d-none d-md-block">Programme:</label>
                                                <select class="form-control" id="kt_datatable_search_programme">
                                                    <option value="all">All</option>
                                                    @foreach ($students as $student)
                                                        <option value="{{ $student->programme }}">
                                                            {{ $student->programme }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--end: Search Form-->
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--begin::Modal-->
                <div id="kt_datatable_review_modal" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl modal-dialog-centered">
                        <div class="modal-content" style="min-height: 590px;">

                            {{-- *********************************BEGIN MODAL********************************* --}}


                            <div class="card card-custom gutter-b">
                                <!--begin::Header-->
                                <div class="card-header card-header-tabs-line">
                                    <div class="card-toolbar">
                                        <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-bold nav-tabs-line-3x"
                                            role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab"
                                                    href="#kt_apps_contacts_view_tab_1">
                                                    <span class="nav-icon mr-2">
                                                        <span class="svg-icon mr-3">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path
                                                                        d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                                                                        fill="#000000" />
                                                                    <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5"
                                                                        r="2.5" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                    <span class="nav-text">Company Details</span>
                                                </a>
                                            </li>
                                            <li class="nav-item mr-3">
                                                <a class="nav-link" data-toggle="tab"
                                                    href="#kt_apps_contacts_view_tab_2">
                                                    <span class="nav-icon mr-2">
                                                        <span class="svg-icon mr-3">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Chat-check.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path
                                                                        d="M6,2 L18,2 C19.6568542,2 21,3.34314575 21,5 L21,19 C21,20.6568542 19.6568542,22 18,22 L6,22 C4.34314575,22 3,20.6568542 3,19 L3,5 C3,3.34314575 4.34314575,2 6,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z"
                                                                        fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>



                                                    <span class="nav-text">Student Details</span>
                                                </a>
                                            </li>
                                            <li class="nav-item mr-3">
                                                <a class="nav-link" data-toggle="tab"
                                                    href="#kt_apps_contacts_view_tab_3">
                                                    <span class="nav-icon mr-2">
                                                        <span class="svg-icon mr-3">
                                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Chat-check.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path
                                                                        d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z"
                                                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path
                                                                        d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z"
                                                                        fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                    <span class="nav-text">Offer Letter</span>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>
                                    <div class="card-toolbar">
                                        <a href="#" class="btn btn-light-primary font-weight-bolder mr-2"
                                            data-dismiss="modal">
                                            <i class="ki ki-long-arrow-back icon-xs"></i>Close</a>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary font-weight-bolder">
                                                <i class="ki ki-check icon-xs"></i>Review</button>
                                            <button type="button"
                                                class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                                <ul class="nav nav-hover flex-column">
                                                    <li class="nav-item">
                                                        <span href="#" class="nav-link" id="btn_review_save"
                                                            style="cursor: pointer">
                                                            <i class="nav-icon flaticon2-add-1"></i>
                                                            <span class="nav-text" data-toggle="popover"
                                                                title="Review &amp; Save" data-html="true"
                                                                data-content="Mark Status as <span class='label label-inline font-weight-bold label-light-success'>Reviewed</span> &amp; Save Company Record to List">Review
                                                                &amp; Save</span>
                                                        </span>
                                                    </li>
                                                    <li class="nav-item">
                                                        <span class="nav-link" id="btn_review"
                                                            style="cursor: pointer">
                                                            <i class="nav-icon flaticon2-checkmark"></i>
                                                            <span class="nav-text" data-toggle="popover"
                                                                title="Review Only" data-html="true"
                                                                data-content="Mark Status as <span class='label label-inline font-weight-bold label-light-success'>Reviewed</span> Only">Review
                                                                Only</span>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body px-0">
                                    <div class="tab-content pt-5">

                                        <div class="tab-pane active" id="kt_apps_contacts_view_tab_1" role="tabpanel">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right" id="org_title">
                                                    Organisation Name:
                                                </label>

                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_org"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Email:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control-plaintext font-weight-bolder"
                                                        id="company_email">
                                                        <a href="mailto:name@rapidtables.com"></a>
                                                    </span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Contact
                                                    Number:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control-plaintext font-weight-bolder">
                                                        <a href="#" id="company_contact"></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Address:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea disabled
                                                        class="form-control form-control-lg form-control-solid"
                                                        id="company_address" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Site
                                                    Supervisor:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_supervisor"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 col-form-label text-right">Designation:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_designation"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Intern's
                                                    Designation:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_internDesignation"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Start
                                                    Internship:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_start"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">End
                                                    Internship:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_end"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 col-form-label text-right">Allowance:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="company_allowance"></span>

                                                </div>
                                            </div>
                                            <div class="form-group row" id="row_jobDesc">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Job
                                                    Description:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea disabled class="form-control" name="input_address"
                                                        id="company_jobDesc" placeholder="Enter Address"
                                                        rows="3"></textarea>
                                                </div>
                                            </div>

                                            {{-- ******************************************************************************* --}}

                                            {{-- <div class="separator separator-dashed my-10"></div> --}}

                                        </div>
                                        <div class="tab-pane" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                                            {{-- <div class="form-group row">
                                                <div class="col-lg-9 col-xl-6 offset-xl-3">
                                                    <h3 class="font-size-h6 mb-5">Account Info:</h3>
                                                </div>
                                            </div> --}}
                                            <!--end::Heading-->
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Photo:</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="image-input image-input-outline image-input-circle"
                                                        id="kt_student_avatar"
                                                        style="background-image: url(assets/media/users/blank.png)">
                                                        <div class="image-input-wrapper" style="background-image: url()">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Full
                                                    Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        type="text" id="student_name"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Student
                                                    ID</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        type="text" id="student_ID"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 text-right col-form-label">Programme</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        type="text" id="student_programme"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 text-right col-form-label">IC/Passport</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group input-group-lg input-group-solid">
                                                        <span type="number"
                                                            class="form-control form-control-lg form-control-solid"
                                                            id="student_IC"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Email</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group input-group-lg input-group-solid">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-at"></i>
                                                            </span>
                                                        </div>
                                                        <span class="form-control form-control-lg form-control-solid"
                                                            id="student_email">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Phone
                                                    Number</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <span class="form-control form-control-lg form-control-solid"
                                                        id="student_phoneNum">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 text-right col-form-label">Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <textarea disabled
                                                        class="form-control form-control-lg form-control-solid"
                                                        id="student_address" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_apps_contacts_view_tab_3" role="tabpanel">

                                        </div>
                                    </div>
                                </div>
                                <!--end::Body-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="kt_datatable_map_modal" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl modal-dialog-centered">
                        <div class="modal-content" style="min-height: 590px;">

                            {{-- *********************************BEGIN MODAL********************************* --}}
                            <div class="card card-custom gutter-b" id="kt_map_card">

                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Modal-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')

    <script>
        let url_applicantList = "{{ route('getApplicantList') }}";
        let url_ApplicantData = "{{ route('getApplicantData') }}";
        let url_actionApplicant = "{{ route('actionApplicant') }}";
        let path_avatar = "{{ asset('profileImages/') }}";
        let url_logbook = "{{ route('logbookCoordinator_page', '') }}";
        let offer_asset = "{{ asset('offer_letters') }}";
    </script>
    <script src="{{ asset('assets/js/pages/crud/ktdatatable/base/applicant_list.js') }}"></script>
@endsection
