{{-- @extends('layouts.test') --}}
@extends('layouts.headerFooter_supervisor')
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                    <!--end::Page Title-->
                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <span class="text-muted font-weight-bold mr-4">Pre-Internship</span>
                    <!--end::Actions-->

                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">

                    {{-- @include('layouts.semesterDropdown', [$semester_code, $semester]) --}}


                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    {{-- ***********************BEGIN TILES***************************** --}}
                    @if (count($students) > 0)
                        @foreach ($students as $student)
                            @if ($students)
                                <div class="col-xl-4">
                                    <!--begin::Card-->
                                    <div class="card card-custom gutter-b card-stretch">
                                        <!--begin::Body-->
                                        <div class="card-body pt-4 d-flex flex-column justify-content-between">
                                            <!--begin::User-->
                                            <div class="d-flex align-items-center mb-7">
                                                <!--begin::Pic-->
                                                <div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
                                                    @if ($student->profile_path)
                                                        <div class="symbol symbol-lg-75">
                                                            <img alt="Pic"
                                                                src="{{ asset('profileImages/' . $student->profile_path) }}">
                                                        </div>
                                                    @else
                                                        <div class="symbol symbol-lg-75 symbol-primary">
                                                            <span
                                                                class="symbol-label font-size-h3 font-weight-boldest">{{ strtoupper(substr($student->name, 0, 2)) }}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <!--end::Pic-->
                                                <!--begin::Title-->
                                                <div class="d-flex flex-column">
                                                    <a href="#"
                                                        class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">{{ $student->name }}</a>
                                                    <span
                                                        class="text-muted font-weight-bold">{{ $student->programme }}</span>
                                                </div>
                                                <!--end::Title-->
                                            </div>

                                            <!--begin::Info-->
                                            <div class="mb-7">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Student ID:</span>
                                                    <span href="#"
                                                        class="text-muted text-hover-primary">{{ $student->studentID }}</span>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Email:</span>
                                                    <span href="#"
                                                        class="text-muted text-hover-primary">{{ $student->User->email }}</span>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-cente my-1">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Phone:</span>
                                                    <span href="#"
                                                        class="text-muted text-hover-primary">{{ $student->phoneNum }}</span>
                                                </div>
                                                @if ($student->Company)
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <span class="text-dark-75 font-weight-bolder mr-2">Company:</span>
                                                        <span
                                                            class="text-muted font-weight-bold">{{ $student->Company->organisation_name }}</span>
                                                    </div>
                                                @else

                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <span class="text-dark-75 font-weight-bolder mr-2">Company:</span>
                                                        <span class="text-muted font-weight-bold">Not Applied</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <!--end::Info-->
                                            <a href="{{ route('studentProfile_page', [$student->id]) }}"
                                                class="btn btn-block btn-sm btn-light-warning font-weight-bolder text-uppercase py-4">more
                                                details</a>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Card-->
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
                            <h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!</h1>
                            <p class="font-weight-boldest display-4">No Assigned Students.</p>
                        </div>
                    @endif


                </div>
                <!--end::Row-->
                <!--begin::Row-->
                <div class="row">

                </div>
                <!--end::Row-->
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection
