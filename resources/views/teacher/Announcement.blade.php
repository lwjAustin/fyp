@extends('layouts.headerFooter_teacher')
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Announcement Page</h5>
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                @include('layouts.semesterDropdown', [$semester_code, $semester])

                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Profile Personal Information-->
                <div class="row">
                    <div class="flex-row-fluid">
                        <div class="col-lg-12">
                            <div class="card card-custom card-collapsed" data-card="true" id="kt_card_3">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">Create Announcement</h3>
                                    </div>
                                    <div class="card-toolbar">
                                        <a href="#" class="btn btn-light-primary font-weight-bold" data-card-tool="toggle"
                                            data-toggle="tooltip" data-placement="top" title="Add New">
                                            <i class="ki ki-plus"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin::Form-->
                                    <form class="form form-label" method="POST"
                                        action="{{ route('createAnnounncement') }}" id="form-add">
                                        @csrf
                                        <div class="card-body">

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Title:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <input type="text" class="form-control" name="input_title"
                                                        id="input_title" placeholder="Enter Title">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 col-form-label text-right">Description:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" data-provide="markdown" rows="10"
                                                        name="input_description" id="input_description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                                    <button type="button" class="btn btn-primary"
                                                        id="btn_add_announcement">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="example example-compact mt-2 gutter-b">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if (count($announcements) >= 1)
                        @foreach ($announcements as $announcement)
                            <div class="col-lg-6">
                                <div class="card card-custom" card_row_id="{{ $announcement->id }}">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="flaticon2-sheet text-primary"></i>

                                            </span>
                                            <h3 class="card-label">{{ $announcement->title }}</h3>

                                            <span class="text-muted font-weight-bold">
                                                <small>{{ Carbon\Carbon::parse($announcement->updated_at)->format('g:i A M d') }}</small>
                                            </span>
                                        </div>
                                        <div class="card-toolbar" modal_row_id="{{ $announcement->id }}">
                                            <a href="#" class="btn btn-icon btn-sm btn-light-success mr-1"
                                                id="btn_show_modal">
                                                <i class="ki ki-bold-sort icon-nm"></i>
                                            </a>
                                            <a href="#" class="btn btn-icon btn-sm btn-light-danger" data-card-tool="remove"
                                                id="btn_delete">
                                                <i class="ki ki-close icon-nm"></i>
                                            </a>
                                        </div>


                                    </div>
                                    <div class="card-body">
                                        {{ Illuminate\Mail\Markdown::parse($announcement->description) }}

                                    </div>
                                </div>

                                <div class="example example-compact mt-2 gutter-b">
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="modal fade" id="announcement_modal" data-backdrop="static" tabindex="-1" role="dialog"
                        aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Announcement</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close" id="btn_close"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form class="form form-label" method="POST" action="#" id="form-edit">
                                        @csrf
                                        <div class="card-body">

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Title:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <input type="text" class="form-control" name="edit_title"
                                                        id="edit_title" placeholder="Enter Title">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label
                                                    class="col-xl-3 col-lg-3 col-form-label text-right">Description:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea class="form-control" data-provide="markdown" rows="10"
                                                        name="edit_description" id="edit_description"></textarea>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal"
                                                        id="btn_cancel_edit">Cancel</button>
                                                    <button type="button" class="btn btn-primary"
                                                        id="btn_edit_announcement">Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="container">
            <div class="d-flex justify-content-lg-center">
                {{ $announcements->links() }}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var row_id = '';
        var edit = false;
        var isChangeDescription = false;

        const form_add = document.getElementById("form-add");
        var validator_add = FormValidation.formValidation(form_add, {
            fields: {
                input_title: {
                    validators: {
                        notEmpty: {
                            message: "Announcement Name Is Required",
                        },
                    },
                },
                input_description: {
                    validators: {
                        notEmpty: {
                            message: "Announcement Description Is Required",
                        },
                    },
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    // rowSelector: ".fv-row",
                    eleInvalidClass: "",
                    eleValidClass: "",
                }),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });


        const form_edit = document.getElementById("form-edit");
        var validator_edit = FormValidation.formValidation(form_edit, {
            fields: {
                edit_title: {
                    validators: {
                        notEmpty: {
                            message: "Announcement Name Is Required",
                        },
                    },
                },
                edit_description: {
                    validators: {
                        notEmpty: {
                            message: "Announcement Description Is Required",
                        },
                    },
                },
            },

            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    // rowSelector: ".fv-row",
                    eleInvalidClass: "",
                    eleValidClass: "",
                }),
                // Validate fields when clicking the Submit button
                submitButton: new FormValidation.plugins.SubmitButton(),
                // Submit the form when all fields are valid
                // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        });


        $("#edit_description").change(function(event) {
            isChangeDescription = true;
        });

        $(".card-toolbar").on("click", "#btn_show_modal", function() {
            row_id = $(this).parent().attr("modal_row_id");
            edit = true;
            console.log(row_id);

            $.ajax({
                type: "POST",
                url: '{{ route('getAnnouncement') }}',
                dataType: "json",
                data: {
                    _token: $('meta[name="_token"]').attr(
                        "content"
                    ),
                    row_id: row_id,
                },
                success: function(res) {


                    $("#edit_title").val(res.title);
                    $("#edit_description").val(res.description);
                    $("#announcement_modal").modal("show");
                    // validator_edit.resetForm(true);

                },
            });
        });


        $(".card-toolbar").on("click", "#btn_delete", function() {
            row_id = $(this).parent().attr("modal_row_id");
            var type = 'delete';
            console.log(row_id);
            // $("[card_row_id='" + row_id + "']").html('');

            Swal.fire({
                text: "Are you sure you would like to delete this?",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, return",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light",
                },
            }).then(function(result) {
                if (result.value) {

                    location.reload(true);

                    $.ajax({
                        type: "POST",
                        url: '{{ route('createAnnounncement') }}',
                        dataType: "json",
                        data: {
                            _token: $('meta[name="_token"]').attr(
                                "content"
                            ),
                            row_id: row_id,
                            type: type,
                        },
                    });

                }
            });



        });



        $("#btn_add_announcement").click(function(event) {
            event.preventDefault();

            if (validator_add) {
                validator_add.validate().then(function(status) {
                    if (status == "Valid") {

                        Swal.fire({
                            title: "Success!",
                            text: "Announcement Added",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {
                                console.log('sdfsdf');
                                $("#form-add").submit();
                            }
                        });

                    } else {
                        Swal.fire({
                            text: "Details are missing, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            }

        });


        $("#btn_edit_announcement").click(function(event) {
            event.preventDefault();
            console.log('jello');
            var type = 'edit';

            if (isChangeDescription == true) {
                console.log('changed');
                var edit_description = $("#edit_description").val();
            } else {
                var edit_description = false;
                console.log('not changed');
            }

            if (validator_edit) {
                validator_edit.validate().then(function(status) {
                    if (status == "Valid") {
                        $.ajax({
                            type: "POST",
                            url: '{{ route('createAnnounncement') }}',
                            dataType: "json",
                            data: {
                                _token: $('meta[name="_token"]').attr(
                                    "content"
                                ),
                                type: type,
                                row_id: row_id,
                                edit_title: $("#edit_title").val(),
                                edit_description: edit_description,

                            },
                            success: function(res) {
                                console.log(res.description);

                                $("[card_row_id='" + row_id + "'] .card-label").html('');

                                $("[card_row_id='" + row_id + "'] .card-label").append(res
                                    .title);

                                if (isChangeDescription == true) {
                                    $("[card_row_id='" + row_id + "'] .card-body").html('');
                                    $("[card_row_id='" + row_id + "'] .card-body").append(res
                                        .description);
                                }

                                isChangeDescription = false;
                            },
                        });

                        Swal.fire({
                            title: "Success!",
                            text: "Announcement Edited",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then(function(result) {
                            if (result.value) {

                                $("#announcement_modal").modal("hide");
                            }
                        });

                    } else {
                        Swal.fire({
                            text: "Details are missing, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            },
                        });
                    }
                });
            }

        });


        $("#btn_cancel_edit, #btn_close").click(function(event) {
            validator_edit.resetForm(true);
        });
    </script>

@endsection
