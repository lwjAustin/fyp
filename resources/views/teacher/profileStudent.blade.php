@extends('layouts.headerFooter_supervisor')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">User Details</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{ $student->name }}</span>
                    </div>
                    <!--end::Search Form-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <a href="{{ route('dashboard_supervisor') }}"
                        class="btn btn-default font-weight-bold btn-sm px-3 font-size-base">Back</a>
                    <!--end::Button-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <!--begin::Card header-->
                    <div class="card-header card-header-tabs-line nav-tabs-line-3x">
                        <!--begin::Toolbar-->
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-bold nav-tabs-line nav-tabs-line-3x">
                                <!--begin::Item-->
                                <li class="nav-item mr-3">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_user_edit_tab_1">
                                        <span class="nav-icon">
                                            <span class="svg-icon">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                    viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path
                                                            d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
                                                            fill="#000000" fill-rule="nonzero"></path>
                                                        <path
                                                            d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
                                                            fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                        <span class="nav-text font-size-lg">Company Details</span>
                                    </a>
                                </li>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <li class="nav-item mr-3">
                                    <a class="nav-link" data-toggle="tab" href="#kt_user_edit_tab_2">
                                        <span class="nav-icon">
                                            <span class="svg-icon">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                    viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path
                                                            d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                                            fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        <path
                                                            d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                                            fill="#000000" fill-rule="nonzero"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                        <span class="nav-text font-size-lg">Student Details</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body">
                        <form class="form" id="kt_form">
                            <div class="tab-content">
                                <!--begin::Tab-->
                                <div class="tab-pane show px-7 active" id="kt_user_edit_tab_1" role="tabpanel">
                                    @if ($company)
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right" id="org_title">
                                                Organisation Name:
                                            </label>

                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_org">{{ $company->organisation_name }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Email:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control-plaintext font-weight-bolder" id="company_email">
                                                    <a href="mailto:{{ $company->email }}">{{ $company->email }}</a>
                                                </span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Contact
                                                Number:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control-plaintext font-weight-bolder">
                                                    <a href="#" id="company_contact">{{ $company->contact_no }}</a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Address:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <textarea disabled class="form-control form-control-lg form-control-solid"
                                                    id="company_address" rows="3">{{ $company->address }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Site
                                                Supervisor:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_supervisor">{{ $company->site_supervisor }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Designation:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_designation">{{ $company->designation }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Intern's
                                                Designation:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_internDesignation">{{ $company->intern_designation }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Start
                                                Internship:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_start">{{ $company->start_internship }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">End
                                                Internship:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_end">{{ $company->end_internship }}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Allowance:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="company_allowance">{{ $company->allowance }}</span>

                                            </div>
                                        </div>
                                        @if ($company->job_desc)
                                            <div class="form-group row" id="row_jobDesc">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Job
                                                    Description:</label>
                                                <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                    <textarea disabled class="form-control" name="input_address"
                                                        id="company_jobDesc" placeholder="Enter Address"
                                                        rows="3">{{ $company->job_desc }}</textarea>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Map:</label>
                                            <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                <iframe width="560" height="412" style="border:0" loading="lazy"
                                                    allowfullscreen
                                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDXf5G5L1zomTZXwpt_kogrSUl93xCkozo&q={{ $coordinate['lat'] }},{{ $coordinate['lng'] }}">
                                                </iframe>
                                            </div>
                                        </div>
                                    @else
                                        <div
                                            class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
                                            <h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!
                                            </h1>
                                            <p class="font-weight-boldest display-4">No Company Applied.</p>
                                        </div>
                                    @endif
                                </div>





                                <div class="tab-pane px-7" id="kt_user_edit_tab_2" role="tabpanel">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Photo:</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <div class="image-input image-input-outline image-input-circle"
                                                id="kt_student_avatar"
                                                style="background-image: url({{ asset('profileImages/' . $student->profile_path) }})">


                                                <div class="image-input-wrapper" style="background-image: url()">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Full
                                            Name</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <span class="form-control form-control-lg form-control-solid" type="text"
                                                id="student_name">{{ $student->name }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Student
                                            ID</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <span class="form-control form-control-lg form-control-solid" type="text"
                                                id="student_ID">{{ $student->studentID }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Programme</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <span class="form-control form-control-lg form-control-solid" type="text"
                                                id="student_programme">{{ $student->programme }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">IC/Passport</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <span type="number" class="form-control form-control-lg form-control-solid"
                                                    id="student_IC">{{ $student->IC }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Email</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-at"></i>
                                                    </span>
                                                </div>
                                                <span class="form-control form-control-lg form-control-solid"
                                                    id="student_email">{{ $student->User->email }}</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Phone
                                            Number</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <span class="form-control form-control-lg form-control-solid"
                                                id="student_phoneNum">{{ $student->phoneNum }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Address</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <textarea disabled class="form-control form-control-lg form-control-solid"
                                                id="student_address" rows="3">{{ $student->address }}</textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <!--begin::Card body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection
