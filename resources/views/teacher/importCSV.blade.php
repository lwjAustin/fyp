{{-- @extends('layouts.test') --}}
@extends('layouts.headerFooter_teacher')
@section('header_title')

@endsection
@section('content')

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Pre-Internship</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Import CSV</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                @include('layouts.semesterDropdown', [$semester_code, $semester])

                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                    <div class="alert-icon">
                        <span class="svg-icon svg-icon-primary svg-icon-xl">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                        d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z"
                                        fill="#000000" opacity="0.3" />
                                    <path
                                        d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z"
                                        fill="#000000" fill-rule="nonzero" />
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </div>
                    <div class="alert-text">Download the example CSV files and try it out.
                        <a class="font-weight-bold" href="{{ route('download_studentCSV') }}" target="_blank">Student
                            List</a>
                        |
                        <a class="font-weight-bold" href="{{ route('download_companyCSV') }}" target="_blank">Company
                            List</a>
                    </div>
                </div>

                {{-- ***********************BEGIN FILE DROP***************************** --}}

                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Import Student List</h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12 text-lg-right">Student List File</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="dropzone dropzone-default dropzone-success dz-clickable"
                                    id="dropzone_studentList">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                        <span class="dropzone-msg-desc">Only csv files are allowed for
                                            upload</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-4">
                                <a href="" class="btn btn-light-primary mr-2" id="import_btn_studentList">
                                    <i class="flaticon2-list-3"></i>
                                    Import CSV
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>

                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Import Company List</h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12 text-lg-right">Company List File</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="dropzone dropzone-default dropzone-success dz-clickable"
                                    id="dropzone_companyList">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                        <span class="dropzone-msg-desc">Only csv files are allowed for
                                            upload</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-4">
                                <a href="" class="btn btn-light-primary mr-2" id="import_btn_companyList">
                                    <i class="flaticon2-list-3"></i>
                                    Import CSV
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Card-->
                <!--begin::Row-->
                <div class="row">
                </div>
                <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')

    <script>
        var uploaded_file = false;

        $('#dropzone_studentList').dropzone({
            autoProcessQueue: false,
            url: '{{ route('import_studentList') }}', // Set the url for your upload script location
            // paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            init: function() {
                this.hiddenFileInput.removeAttribute('multiple');
                this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
            },
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            // acceptedFiles: "image/*,application/pdf,.psd",
            acceptedFiles: ".csv",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            accept: function(file, done) {
                if (this.files.length == 1) {
                    uploaded_file = true;
                } else {
                    uploaded_file = false;
                }

                done();
            },
            removedfile: function(file) {
                file.previewElement.remove();

                if (this.files.length == 1) {
                    uploaded_file = true;
                } else {
                    uploaded_file = false;
                }
                done();

            },

        });


        $('#dropzone_companyList').dropzone({
            autoProcessQueue: false,
            url: '{{ route('import_companyList') }}', // Set the url for your upload script location
            // paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            init: function() {
                this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
            },
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            // acceptedFiles: "image/*,application/pdf,.psd",
            acceptedFiles: ".csv",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            accept: function(file, done) {
                if (this.files.length == 1) {
                    uploaded_file = true;
                } else {
                    uploaded_file = false;
                }

                done();
            },
            removedfile: function(file) {
                file.previewElement.remove();

                if (this.files.length == 1) {
                    uploaded_file = true;
                } else {
                    uploaded_file = false;
                }
                done();

            },

        });

        $("#import_btn_studentList").click(function(e) {
            e.preventDefault();
            if (uploaded_file == true) {
                Dropzone.forElement("#dropzone_studentList").processQueue();

                Swal.fire({
                    title: "Success!",
                    text: "Student List Imported!",
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = "{{ route('studentList_page') }}";
                    }
                });

            }
            if (uploaded_file == false) {

                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please Choose A File!',
                    timer: 1600
                })
            }
        });


        $("#import_btn_companyList").click(function(e) {
            e.preventDefault();
            if (uploaded_file == true) {
                Dropzone.forElement("#dropzone_companyList").processQueue();

                Swal.fire({
                    title: "Success!",
                    text: "Student List Imported!",
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                }).then(function(result) {
                    if (result.value) {
                        window.location.href = "{{ route('companyList_page') }}";
                    }
                });

            }
            if (uploaded_file == false) {

                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please Choose A File!',
                    timer: 1600
                })
            }
        });
    </script>

@endsection
