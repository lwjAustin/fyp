@extends('layouts.headerFooter_teacher')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Student List</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pre-Internship</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Student List</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                @include('layouts.semesterDropdown', [$semester_code, $semester])

            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Company List
                                <span class="d-block text-muted pt-2 font-size-sm">Update, Delete or Add New Record</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">

                            <!--begin::Button-->
                            <button class="btn btn-primary font-weight-bolder" id="new_record">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <circle fill="#000000" cx="9" cy="15" r="6" />
                                            <path
                                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                                fill="#000000" opacity="0.3" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>New Record</button>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-xl-8">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="Search..."
                                                    id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-2 my-md-0">
                                            <div class="d-flex align-items-center">
                                                <label class="mr-3 mb-0 d-none d-md-block">Level:</label>
                                                <select class="form-control" id="kt_datatable_search_level">
                                                    <option value="all">All</option>
                                                    <option value="diploma">Diploma</option>
                                                    <option value="degree">Degree</option>
                                                    <option value="others">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--end: Search Form-->
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--begin::Card-->
                <!--begin::Modal-->
                <div id="kt_datatable_modal" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl modal-dialog-centered">
                        <div class="modal-content" style="min-height: 590px;">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title" id="modal_title">
                                    </h3>
                                </div>
                                <!--begin::Form-->
                                <form id="form-update">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="example-url-input" class="col-2 col-form-label">Company</label>
                                            <div class="col-10">
                                                <input class="form-control" id="company-input" type="text" value=""
                                                    name="company">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-url-input" class="col-2 col-form-label">Person
                                                Incharge</label>
                                            <div class="col-10">
                                                <input class="form-control" id="person-input" type="text" value=""
                                                    name="person_incharge" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-email-input" class="col-2 col-form-label">Email</label>
                                            <div class="col-10">
                                                <input class="form-control" id="email-input" type="email" value=""
                                                    name="email" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-2 col-form-label">Position</label>
                                            <div class="col-10">
                                                <input class="form-control" id="position-input" type="text" value=""
                                                    name="position" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-2 col-form-label">Contact
                                                No</label>
                                            <div class="col-10">
                                                <input class="form-control" id="contact_no-input" type="tel" value=""
                                                    name="contact_no">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-url-input" class="col-2 col-form-label">Level</label>
                                            <div class="col-10">
                                                <input class="form-control" id="level-input" type="text" value=""
                                                    name="level" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-url-input" class="col-2 col-form-label">Address</label>
                                            <div class="col-10">
                                                <textarea class="form-control" id="address-input" name="address" rows="3"
                                                    autocomplete="off"></textarea>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="example-datetime-local-input" class="col-2 col-form-label">Date
                                                and
                                                time</label>
                                            <div class="col-10">
                                                <input class="form-control" type="datetime-local"
                                                    value="2011-08-19T13:45:00" id="example-datetime-local-input">
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div class="card-footer flex-center">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-10">
                                                <button type="reset" class="btn btn-secondary"
                                                data-dismiss="modal">Cancel</button>
                                                <button class="btn btn-success mr-2" id="update_details_btn">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Modal-->
                <!--end::Modal-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('script')

    <script>
        let url_companyList = "{{ route('getCompanyList') }}";
        let url_showDetails = "{{ route('showDetails') }}";
        let url_updateDetails = "{{ route('updateDetails') }}";
        let url_deleteDetails = "{{ route('deleteDetails') }}";
    </script>
    <script src="{{ asset('assets/js/pages/crud/ktdatatable/base/company_list.js') }}"></script>
@endsection
