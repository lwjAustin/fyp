@extends('layouts.headerFooter_teacher')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">{{ $student->name }} ({{ $student->studentID }})
                        </h5>
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <a href="{{ route('applicantList_page') }}"
                        class="btn btn-light-warning font-weight-bolder btn-sm">Back</a>
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Profile Personal Information-->
                @foreach ($logbooks as $logbook)
                    <div class="row">
                        <div class="flex-row-fluid">
                            <div class="col-lg-12">
                                <div class="card card-custom card-collapsed" data-card="true" id="kt_card_3">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <h3 class="card-label">Logbook Week {{ intval($logbook->week) }}</h3>
                                        </div>
                                        <div class="card-toolbar">
                                            <a href="#" class="btn btn-light-primary font-weight-bold"
                                                data-card-tool="toggle" data-toggle="tooltip" data-placement="top"
                                                title="Expand">
                                                <i class="ki ki-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <!--begin::Form-->
                                        <form class="form form-label" method="POST" action="{{ route('createLogbook') }}"
                                            id="form-add">
                                            @csrf
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Task:</label>
                                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                        <textarea disabled class="form-control" name="input_task"
                                                            id="input_task" placeholder="Enter Task"
                                                            rows="3">{{ $logbook->task }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Work
                                                        Done:</label>
                                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                        <textarea disabled class="form-control" name="input_work_done"
                                                            id="input_work_done" placeholder="Enter Work Done"
                                                            rows="5">{{ $logbook->work_done }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Record of
                                                        Discussion:</label>
                                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                        <textarea disabled class="form-control" name="input_record_disc"
                                                            id="input_record_disc" placeholder="Enter Record of Discussion"
                                                            rows="5">{{ $logbook->record_disc }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Problems
                                                        Encountered:</label>
                                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                        <textarea disabled class="form-control" name="input_problems"
                                                            id="input_problems" placeholder="Enter Problems Encountered"
                                                            rows="5">{{ $logbook->problems }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-right">Further
                                                        Notes:</label>
                                                    <div class="col-lg-9 col-xl-6" style="padding-left: 3%">
                                                        <textarea disabled class="form-control" name="input_notes"
                                                            id="input_notes" placeholder="Enter Futher Notes"
                                                            rows="4">{{ $logbook->notes }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <div class="container">
            <div class="d-flex justify-content-lg-center">
                {{-- {{ $announcements->links() }} --}}
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection
